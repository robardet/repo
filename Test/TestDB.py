'''
Created on Jul 24, 2020

@author: user
'''
import unittest
import conversion
from Controller import Plotgraph
import RecordDB
from RecordDB import filterbytime_footprint
import random
import os
import shutil


class Test(unittest.TestCase):
    
    
    def recherchespatialedateplot(self):
        
        p = Plotgraph()
        s1 = p.recherchespatialeplot("*",["lte","gte"], [20,-80], ["lte","gte"], [20,-80])
        self.assertEqual(s1, "254 Résultats trouvés")
        
    """
    def testRechercheSpatialePlot(self):
        p = Plotgraph()
        a1=RecordDB.resetDB()
        RecordDB.enregistrer_donnee("/home/user/Documents/Test")
        a2 = RecordDB.getallindex()
        for a in a1:
            self.assertIn(a, a2, a+"n'était pas dans la liste d'index et y est maintenant")
              
        data  = p.recherchespatialedateplot("*",optime=["lte","gte"], crittime=["2020-08-11","2020-06-11"])
        print(data)
        
        alldata = RecordDB.matchall("*")
        print(alldata)
        
        s = p.recherchespatialeplot("*",["lte,gte"], [20,-20], ["lte,gte"], [20,-20])
        self.assertEqual(s,"probleme avec le nombre d'arguments")

        s1 = p.recherchespatialeplot("*",["lte","gte"], [20,-20], ["lte","gte"], [20,-20])
        self.assertEqual(s1,"0 fichier ne correspond à votre requete")
        
        s4 = filterbytime_footprint ("*",["lte","gte"], [20,-80], ["lte","gte"], [20,-80])
        s2 = p.recherchespatialeplot("*",["lte","gte"], [20,-80], ["lte","gte"], [20,-80])
        s3 = p.recherchespatialedateplot("*",["lte","gte"], [20,-80], ["lte","gte"], [20,-80])
        self.assertEqual(s4, s3)
        self.assertEqual(s3, s2)
        
        
        
        for i in range (0,1):
            r1 = random.random()*100
            r2 = random.random()*-100
            if r2>r1:
                t=r1
                r1=r2
                r2=t
            
            
            
            data2 = p.recherchespatialedateplot(index = "*", opra=["gte","lte"], critra = [r2, r1], opdec = ["gte","lte"], critdec = [r2, r1])
            print(data2)
            data2b = p.recherchespatialeplot(index = "*", opra=["gte","lte"], critra = [r2, r1], opdec = ["gte","lte"], critdec = [r2, r1])
            print(data2b)

        
            data1b = p.recherchespatialedateplot(index = "*", opra=["gte","lte"], critra = [r2, r1], opdec = ["gte","lte"], critdec = [r2, r1], optime = ["lte","gte"], crittime = ["2020-08-11","2020-06-11"])
            print("recherchespatialedateplot = "+data1b)
            data1= filterbytime_footprint(index = "*", opspacera=["gte","lte"], critspacera = [r2, r1], opspacedec = ["gte","lte"], critspacedec = [r2,r1], optime = ["lte","gte"], crit_time = ["2020-08-11","2020-06-11"])
            print("filterbytime_footprint = "+data1)
            self.assertGreaterEqual(int(data.split(" ")[0]), int(data1.split(" ")[0]))
            self.assertEqual(data1, data1b, "Les deux recherches identiques ne renvoient pas le même nombre de résultat")
            
            
            self.assertEqual(data2, data2b)

            data2ba = filterbytime_footprint(index = "*", opspacera=["gte","lte"], critspacera = [r2, r1], opspacedec = ["gte","lte"], critspacedec = [r2,r1])
            print(data2ba)
            data2bb = RecordDB.recherchespatiale(index = "*", opra=["gte","lte"], critra = [r2, r1], opdec = ["gte","lte"], critdec = [r2,r1])
            print(data2bb)
            self.assertEqual(data2ba, data2bb)
            self.assertEqual(data2, data2ba)
        """
        
        
    """def testBD(self):
        if os.path.exists("/home/user/Documents/ResultatRecherche/"):
            shutil.rmtree("/home/user/Documents/ResultatRecherche/")
        field =[]
        field.append("Data.TileIndex")
        field.append("Data.ObservationId")
        index = {}
        index["Data.TileIndex"] = ["dpdmertile"]
        index["Data.ObservationId"] = ["dpdvisstackedframe"]
        op = {}
        op["Data.TileIndex"]= ["gte","lte"]
        op["Data.ObservationId"] = ["gte","lte"]
        crit = {}
        crit["Data.TileIndex"] = [18,18]
        crit["Data.ObservationId"] = [401,401]
        r1 = RecordDB.requeteEuclid(field,index,op,crit)
        
        arb = RecordDB.listdirectory("/home/user/Documents/ResultatRecherche")
        self.assertEqual("Upperleft_82.4_-29.4_Size_0.2"+".xml", arb[0].split("/")[-1])
        self.assertEqual("EUC_DUMMY_VIS_FILE_401_2018-07-30T00:00:00.000"+".xml", arb[1].split("/")[-1])
        
        
        
    def testRequete2(self):
        if os.path.exists("/home/user/Documents/ResultatRecherche/"):
            shutil.rmtree("/home/user/Documents/ResultatRecherche/")
        index = {}
        op = {}
        crit = {}
        index["Data.ObservationId"] = "DpdVisStackedFrame"
        op["Data.ObservationId"] = ["gte","lte"]
        crit["Data.ObservationId"] = [401,401]
        RecordDB.recherchespatiale(["dpdmertile"], ["gt","lt"], [81.9, 82.3], ["lt","gt"], [-28.9, -29.3])
        RecordDB.requeteEuclid(["Data.ObservationId"], index, op, crit)
        arb = RecordDB.listdirectory("/home/user/Documents/ResultatRecherche")
        self.assertEqual("EUC_DUMMY_VIS_FILE_401_2018-07-30T00:00:00.000"+".xml", arb[0].split("/")[-1])
        #self.assertEqual(len(arb),2)
     """   
        


        
        
        
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testfilterbytime_footprint']
    unittest.main()


