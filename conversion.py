'''
Created on Jun 5, 2020

@author: user
'''
from xmljson import yahoo
import xmljson
from json import dumps
from xml.etree.ElementTree import fromstring, tostring
from json2xml import json2xml
from json2xml.utils import readfromjson, readfromstring, readfromurl
import dicttoxml
from xml.dom.minidom import parseString
import xml.etree.ElementTree as ET
import os
from elasticsearch import Elasticsearch
from elasticsearch.client import IndicesClient
from elasticsearch.client.cat import CatClient
from lxml import etree


"""class y (xmljson.yahoo):
    def __init__(self, **kwargs):
        kwargs.setdefault('xml_fromstring', True)
        super(y, self).__init__(text_content='content', simple_text=True, **kwargs)"""

class Myjson2xml (json2xml.Json2xml) :
    
    def to_xml(self):
        """
        Convert to xml using dicttoxml.dicttoxml and then pretty print it.
        """
        if self.data:
            xml_data = dicttoxml.dicttoxml(self.data, custom_root=self.wrapper, root=False, attr_type = False )
            if self.pretty:
                return parseString(xml_data).toprettyxml()
            return xml_data
        return None

"""def creerpath(path):
    if not os.path.exists("/home/user/Documents/ResultatRecherche/"):
        os.makedirs("/home/user/Documents/ResultatRecherche/")
"""
def xmltojson(path_to_xml, path_to_json):

    xml1 = open(path_to_xml, "r")
    contenu = xml1.read()
    if contenu[:39] == "<?xml version=\"1.0\" encoding=\"utf-8\"?>":
        contenu = contenu.lstrip("<?xml version=\"1.0\" encoding=\"utf-8\"?>")
    else :
        if contenu[:22] == "<?xml version=\"1.0\" encoding=\"utf-8\"?>":
                contenu = contenu.lstrip("<?xml version=\"1.0\" ?>")
    json1 = dumps(yahoo.data(fromstring(contenu)))
    fjson1 = open(path_to_json, "w")
    fjson1.write(json1)
    
    
    xml1.close()
    fjson1.close()

    
def jsontoxml(path_to_json, path_to_xml):
    xml = open(path_to_xml, "w")
    data = readfromjson(path_to_json)
    strxml = Myjson2xml(data).to_xml()
    root = ET.fromstring(strxml)
    root.tag = root.attrib['name']
    l = root.tag.split("}")
    root.tag = l[-1]
    root.tag = "ns1:" + root.tag
    root.attrib['name'] = root.attrib['name'].lstrip("{")
    root.attrib['name'] = root.attrib['name'].rstrip(root.tag)
    root.attrib['name'] = root.attrib['name'].rstrip("}")
    
    l = strxml.split("<Header>")
    l[1] = l[1].rstrip("</key> \n")

    strxml = "<?xml version=\"1.0\" ?>\n<" + root.tag + " xmlns:ns1=\"" + root.attrib['name'] + "\">\n\t<Header>" + l[1] + ">\n</" + root.tag + ">"
    
    xml.write(strxml)
    xml.close
    
    
def jsonstringtoxml(json_data, path_to_xml):
    xml = open(path_to_xml, "w")
    data = readfromstring(json_data)
    strxml = Myjson2xml(data).to_xml()
    root = ET.fromstring(strxml)
    root.tag = root.attrib['name']
    l = root.tag.split("}")
    root.tag = l[-1]
    root.tag = "ns1:" + root.tag
    root.attrib['name'] = root.attrib['name'].lstrip("{")
    root.attrib['name'] = root.attrib['name'].rstrip(root.tag)
    root.attrib['name'] = root.attrib['name'].rstrip("}")
    
    l = strxml.split("<Header>")
    l[1] = l[1].rstrip("</key> \n")

    strxml = "<?xml version=\"1.0\" ?>\n<" + root.tag + " xmlns:ns1=\"" + root.attrib['name'] + "\">\n\t<Header>" + l[1] + ">\n</" + root.tag + ">"
    
    xml.write(strxml)
    xml.close

    
def deleteitem(path_to_xml):
    deletenode(path_to_xml, "item")

def deletenode(path_to_xml, node):
    """
    @path_to_xml fichier xml a modifier
    @node noeud a supprimer en rattachant ses noeuds fils a son noeud pere
    """
    l = open(path_to_xml, "r")
    tree = ET.parse(l)
    root = tree.getroot()
    for item in root.findall(".//" + node + "/.."):
        for children in item.getchildren():
            for position in children.getchildren():
                item.append(position)
            item.remove(children)
    l.close()
    tree.write(path_to_xml)
    file = open(path_to_xml,"r")
    st = file.read()
    file.close
    st = st.replace("ns0", "ns1")
    file = open(path_to_xml,"w")
    file.write(st)
    file.close

def getallpath(index, path_to_xml):
    es = Elasticsearch()
    es.ic= IndicesClient(es)
    escat = CatClient(es)
    dic = {}
    if (index=="*" or index=="_all"): 
        index = escat.indices(index = "_all", h="index").split("\n")
        del(index[-1])
    for ind in index:
        dic[ind] = es.get_source(index = ind, id = 0)
        prod = str(dic[ind]).replace("'","\"")
        prod = prod.replace("False", "false")
        prod = prod.replace("True", "true")
        print (prod)
        jsonstringtoxml(prod, path_to_xml + "/" +ind + ".xml")
        deleteitem(path_to_xml+ "/" +ind+ ".xml")
    print(dic)



"""def allpath(path_to_xml):
    p = []
    l = open(path_to_xml, "r")
    tree = ET.parse(l)
    root = tree.getroot()
    for node in root.findall(".//*" ):
        if (node.hasChildrenNodes() == False):
            p.append(node.xpath())
    print(p)
    l.close()


allpath("/home/user/Documents/fields/dpdspepfoutputcatalog.xml")"""