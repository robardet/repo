'''
Created on Jun 23, 2020

@author: user
'''
from PyQt5 import QtCore, QtWidgets
from Interface import Ui_MainWindow
from elasticsearch.client.cat import CatClient
from PyQt5.QtWidgets import QApplication
import sys  
from matplotlib import pyplot as plt
from matplotlib.widgets import Button
from matplotlib.lines import Line2D as l
from elasticsearch import Elasticsearch
import os.path 
import shutil
import RecordDB
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from Mapping import colorindex


        
        
class MyWindow(Ui_MainWindow):
    typechamp = []
    typeprod = []

    def __init__(self):
        super(MyWindow,self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.pushButton_3.clicked.connect(self.action_bouton)
        self.ui.Inverser.clicked.connect(self.inverserselection)
        self.ui.pushButton_2.clicked.connect(self.validerindex)
        self.ui.pushButton.clicked.connect(self.addfield)
        
    def inverserselection(self):
        self.ui.checkdpdextcalibratedframe.setChecked(not self.ui.checkdpdextcalibratedframe.isChecked())
        self.ui.checkdpdextsingleepochframe.setChecked(not self.ui.checkdpdextsingleepochframe.isChecked())
        self.ui.checkdpdextstackedframe.setChecked(not self.ui.checkdpdextstackedframe.isChecked())
        self.ui.checkdpdmerfinalcatalog.setChecked(not self.ui.checkdpdmerfinalcatalog.isChecked())
        self.ui.checkdpdmermosaic.setChecked(not self.ui.checkdpdmermosaic.isChecked())
        self.ui.checkdpdmersegmentationmap.setChecked(not self.ui.checkdpdmersegmentationmap.isChecked())
        self.ui.checkdpdmertile.setChecked(not self.ui.checkdpdmertile.isChecked())
        self.ui.checkdpdnircalibratedframe.setChecked(not self.ui.checkdpdnircalibratedframe.isChecked())
        self.ui.checkdpdnirstackedframe.setChecked(not self.ui.checkdpdnirstackedframe.isChecked())
        self.ui.checkdpdnisprawframe.setChecked(not self.ui.checkdpdnisprawframe.isChecked())
        self.ui.checkdpdsircombinedspectra.setChecked(not self.ui.checkdpdsircombinedspectra.isChecked())
        self.ui.checkdpdspepfoutputcatalog.setChecked(not self.ui.checkdpdspepfoutputcatalog.isChecked())
        self.ui.checkdpdtrueuniverseoutput.setChecked(not self.ui.checkdpdtrueuniverseoutput.isChecked())
        self.ui.checkdpdviscalibratedframe.setChecked(not self.ui.checkdpdviscalibratedframe.isChecked())
        self.ui.checkdpdvismasterbiasframe.setChecked(not self.ui.checkdpdvismasterbiasframe.isChecked())
        self.ui.checkdpdvisstackedframe.setChecked(not self.ui.checkdpdvisstackedframe.isChecked())
        self.ui.checkdpdvisstackedframecatalog.setChecked(not self.ui.checkdpdvisstackedframecatalog.isChecked())
        self.ui.checkdpdphzpfoutputcatalog.setChecked(not self.ui.checkdpdphzpfoutputcatalog.isChecked())
        
    def addfield(self):
        text = self.ui.comboBox.currentText()
        xbox = self.ui.comboBox.x()
        ybox = self.ui.comboBox.y()
        xbut = self.ui.pushButton.x()
        ybut = self.ui.pushButton.y()
        if text == "Exact Date":
            self.ui.labeldate = QtWidgets.QLabel(self.ui.groupBox_2)
            self.ui.labeldate.setGeometry(QtCore.QRect(30, ybut, 81, 21))
            self.ui.labeldate.setLocale(QtCore.QLocale(QtCore.QLocale.French, QtCore.QLocale.France))
            self.ui.labeldate.setText("Date Exacte")
            self.ui.calendrier = QtWidgets.QCalendarWidget(self.ui.groupBox_2)
            self.ui.calendrier.setGeometry(QtCore.QRect(xbox, ybut, 240, 140))
            self.ui.calendrier.setLocale(QtCore.QLocale(QtCore.QLocale.French, QtCore.QLocale.France))
            self.ui.deletedate = QtWidgets.QPushButton(self.ui.groupBox_2)
            self.ui.deletedate.setLocale(QtCore.QLocale(QtCore.QLocale.French, QtCore.QLocale.France))
            self.ui.deletedate.setGeometry(xbox+400, ybut, 30,30)
            self.ui.deletedate.setStyleSheet("background-color: red")
            self.ui.deletedate.setText("X")
            self.ui.comboBox.move(xbox,ybox+180)
            self.ui.pushButton.move(xbut,ybut+180)
            self.ui.deletedate.clicked.connect(self.delDate)
            self.ui.comboBox.removeItem(self.ui.comboBox.currentIndex())
            self.ui.comboBox.removeItem(self.ui.comboBox.currentIndex())
            self.ui.labeldate.show()
            self.ui.calendrier.show()
            self.ui.deletedate.show()
            self.typechamp.append("DE")
        if text == "Time Range" :
            self.ui.labeldate1 = QtWidgets.QLabel(self.ui.groupBox_2)
            self.ui.labeldate1.setGeometry(QtCore.QRect(30, ybut, 100, 20))
            self.ui.labeldate1.setLocale(QtCore.QLocale(QtCore.QLocale.French, QtCore.QLocale.France))
            self.ui.labeldate1.setText("Date de début")
            self.ui.calendrier1 = QtWidgets.QCalendarWidget(self.ui.groupBox_2)
            self.ui.calendrier1.setGeometry(QtCore.QRect(xbox-140, ybut, 240, 140))
            self.ui.calendrier1.setLocale(QtCore.QLocale(QtCore.QLocale.French, QtCore.QLocale.France))
            self.ui.labeldate2 = QtWidgets.QLabel(self.ui.groupBox_2)
            self.ui.labeldate2.setGeometry(QtCore.QRect(240 + xbox, ybut, 100, 20))
            self.ui.labeldate2.setLocale(QtCore.QLocale(QtCore.QLocale.French, QtCore.QLocale.France))
            self.ui.labeldate2.setText("Date de fin")
            self.ui.calendrier2 = QtWidgets.QCalendarWidget(self.ui.groupBox_2)
            self.ui.calendrier2.setGeometry(QtCore.QRect(370 +xbox, ybut, 240, 140))
            self.ui.calendrier2.setLocale(QtCore.QLocale(QtCore.QLocale.French, QtCore.QLocale.France))
            self.ui.deletedate2 = QtWidgets.QPushButton(self.ui.groupBox_2)
            self.ui.deletedate2.setLocale(QtCore.QLocale(QtCore.QLocale.French, QtCore.QLocale.France))
            self.ui.deletedate2.setGeometry(xbox+650, ybut, 30,30)
            self.ui.deletedate2.setStyleSheet("background-color: red")
            self.ui.deletedate2.setText("X")
            self.ui.deletedate2.clicked.connect(self.delDate2)
            self.ui.comboBox.removeItem(self.ui.comboBox.currentIndex())
            self.ui.comboBox.removeItem(self.ui.comboBox.currentIndex()-1)
            self.ui.comboBox.move(xbox,ybox+180)
            self.ui.pushButton.move(xbut,ybut+180)
            self.ui.labeldate1.show()
            self.ui.calendrier1.show()
            self.ui.labeldate2.show()
            self.ui.calendrier2.show()
            self.ui.deletedate2.show()
            self.typechamp.append("IT")
            
            
            
        if text == "Right Ascencion Range":
            self.ui.labelra1 = QtWidgets.QLabel(self.ui.groupBox_2)
            self.ui.labelra1.setGeometry(QtCore.QRect(30, ybut, 100, 21))
            self.ui.labelra1.setText("Ra minimum")
            self.ui.ra1 = QtWidgets.QLineEdit(self.ui.groupBox_2)
            self.ui.ra1.setGeometry(QtCore.QRect(xbox-140, ybut, 240, 40))
            self.ui.labelra2 = QtWidgets.QLabel(self.ui.groupBox_2)
            self.ui.labelra2.setGeometry(QtCore.QRect(240+xbox, ybut, 100, 21))
            self.ui.labelra2.setText("Ra maximum")
            self.ui.ra2 = QtWidgets.QLineEdit(self.ui.groupBox_2)
            self.ui.ra2.setGeometry(QtCore.QRect(xbox+370, ybut, 240, 40))
            self.ui.deletera = QtWidgets.QPushButton(self.ui.groupBox_2)
            self.ui.deletera.setLocale(QtCore.QLocale(QtCore.QLocale.French, QtCore.QLocale.France))
            self.ui.deletera.setGeometry(xbox+650, ybut, 30,30)
            self.ui.deletera.setStyleSheet("background-color: red")
            self.ui.deletera.setText("X")
            self.ui.comboBox.removeItem(self.ui.comboBox.currentIndex())
            self.ui.labelra2.show()
            self.ui.ra2.show()
            self.ui.labelra1.show()
            self.ui.ra1.show()
            self.ui.deletera.show()
            self.ui.comboBox.move(xbox,ybox+100)
            self.ui.pushButton.move(xbut,ybut+100)
            self.typechamp.append("RA")
            self.ui.deletera.clicked.connect(self.delRa)
            
            
        if text == "Declination Range":
            self.ui.labeldec1 = QtWidgets.QLabel(self.ui.groupBox_2)
            self.ui.labeldec1.setGeometry(QtCore.QRect(30, ybut, 100, 21))
            self.ui.labeldec1.setText("Dec minimum")
            self.ui.dec1 = QtWidgets.QLineEdit(self.ui.groupBox_2)
            self.ui.dec1.setGeometry(QtCore.QRect(xbox-140, ybut, 240, 40))
            self.ui.labeldec2 = QtWidgets.QLabel(self.ui.groupBox_2)
            self.ui.labeldec2.setGeometry(QtCore.QRect(240+xbox, ybut, 100, 21))
            self.ui.labeldec2.setText("Dec maximum")
            self.ui.dec2 = QtWidgets.QLineEdit(self.ui.groupBox_2)
            self.ui.dec2.setGeometry(QtCore.QRect(xbox+370, ybut, 240, 40))
            self.ui.deletedec = QtWidgets.QPushButton(self.ui.groupBox_2)
            self.ui.deletedec.setLocale(QtCore.QLocale(QtCore.QLocale.French, QtCore.QLocale.France))
            self.ui.deletedec.setGeometry(xbox+650, ybut, 30,30)
            self.ui.deletedec.setStyleSheet("background-color: red")
            self.ui.deletedec.setText("X")
            self.ui.comboBox.removeItem(self.ui.comboBox.currentIndex())
            self.ui.labeldec2.show()
            self.ui.dec2.show()
            self.ui.labeldec1.show()
            self.ui.dec1.show()
            self.ui.deletedec.show()
            self.ui.comboBox.move(xbox,ybox+100)
            self.ui.pushButton.move(xbut,ybut+100)
            self.typechamp.append("Dec")
            self.ui.deletedec.clicked.connect(self.delDec)
            
        if len(self.typechamp) == 3:
            self.ui.comboBox.hide()
            self.ui.pushButton.hide()
        st = ""
        for i in range(0,len(self.typechamp)):
            st+= self.typechamp[i]
        self.ui.labelError.setText(st)
        
        
        
    def delDate(self):
        self.ui.comboBox.show()
        self.ui.pushButton.show()
        self.ui.comboBox.addItem("Date Exacte")
        self.ui.comboBox.addItem("Intervalle de Temps")
        if "DE" == self.typechamp[-1] :
            self.ui.labeldate.hide()
            self.ui.calendrier.hide()
            self.ui.deletedate.hide()
        elif "DE" == self.typechamp[-2] :
            self.ui.labeldate.hide()
            self.ui.calendrier.hide()
            self.ui.deletedate.hide()
            if self.typechamp[-1] == "Dec" :
                self.ui.labeldec2.move(self.ui.labeldec2.x(),self.ui.labeldec2.y()-180)
                self.ui.dec2.move(self.ui.dec2.x(),self.ui.dec2.y()-180)
                self.ui.labeldec1.move(self.ui.labeldec1.x(),self.ui.labeldec1.y()-180)
                self.ui.dec1.move(self.ui.dec1.x(),self.ui.dec1.y()-180)
                self.ui.deletedec.move(self.ui.deletedec.x(),self.ui.deletedec.y()-180)
            else :
                self.ui.labelra2.move(self.ui.labelra2.x(),self.ui.labelra2.y()-180)
                self.ui.ra2.move(self.ui.ra2.x(),self.ui.ra2.y()-180)
                self.ui.labelra1.move(self.ui.labelra1.x(),self.ui.labelra1.y()-180)
                self.ui.ra1.move(self.ui.ra1.x(),self.ui.ra1.y()-180)
                self.ui.deletera.move(self.ui.deletera.x(),self.ui.deletera.y()-180)
        elif "DE" == self.typechamp[-3]:
            self.ui.labeldate.hide()
            self.ui.calendrier.hide()
            self.ui.deletedate.hide()
            self.ui.labeldec2.move(self.ui.labeldec2.x(),self.ui.labeldec2.y()-180)
            self.ui.dec2.move(self.ui.dec2.x(),self.ui.dec2.y()-180)
            self.ui.labeldec1.move(self.ui.labeldec1.x(),self.ui.labeldec1.y()-180)
            self.ui.dec1.move(self.ui.dec1.x(),self.ui.dec1.y()-180)
            self.ui.deletedec.move(self.ui.deletedec.x(),self.ui.deletedec.y()-180)
            self.ui.labelra2.move(self.ui.labelra2.x(),self.ui.labelra2.y()-180)
            self.ui.ra2.move(self.ui.ra2.x(),self.ui.ra2.y()-180)
            self.ui.labelra1.move(self.ui.labelra1.x(),self.ui.labelra1.y()-180)
            self.ui.ra1.move(self.ui.ra1.x(),self.ui.ra1.y()-180)
            self.ui.deletera.move(self.ui.deletera.x(),self.ui.deletera.y()-180)
        self.ui.pushButton.move(self.ui.pushButton.x(),self.ui.pushButton.y()-180)
        self.ui.comboBox.move(self.ui.comboBox.x(),self.ui.comboBox.y()-180)
        self.typechamp.remove("DE")
        st = ""
        for i in range(0,len(self.typechamp)):
            st+= self.typechamp[i]
        self.ui.labelError.setText(st)
    
    def delDate2(self):
        self.ui.comboBox.show()
        self.ui.pushButton.show()
        self.ui.comboBox.addItem("Date Exacte")
        self.ui.comboBox.addItem("Intervalle de Temps")
        if "IT" == self.typechamp[-1] :
            self.ui.labeldate1.hide()
            self.ui.calendrier1.hide()
            self.ui.labeldate2.hide()
            self.ui.calendrier2.hide()
            self.ui.deletedate2.hide()
        elif "IT" == self.typechamp[-2] :
            self.ui.labeldate1.hide()
            self.ui.calendrier1.hide()
            self.ui.labeldate2.hide()
            self.ui.calendrier2.hide()
            self.ui.deletedate2.hide()
            if self.typechamp[-1] == "Dec" :
                self.ui.labeldec2.move(self.ui.labeldec2.x(),self.ui.labeldec2.y()-180)
                self.ui.dec2.move(self.ui.dec2.x(),self.ui.dec2.y()-180)
                self.ui.labeldec1.move(self.ui.labeldec1.x(),self.ui.labeldec1.y()-180)
                self.ui.dec1.move(self.ui.dec1.x(),self.ui.dec1.y()-180)
                self.ui.deletedec.move(self.ui.deletedec.x(),self.ui.deletedec.y()-180)
            else :
                self.ui.labelra2.move(self.ui.labelra2.x(),self.ui.labelra2.y()-180)
                self.ui.ra2.move(self.ui.ra2.x(),self.ui.ra2.y()-180)
                self.ui.labelra1.move(self.ui.labelra1.x(),self.ui.labelra1.y()-180)
                self.ui.ra1.move(self.ui.ra1.x(),self.ui.ra1.y()-180)
                self.ui.deletera.move(self.ui.deletera.x(),self.ui.deletera.y()-180)
        elif "IT" == self.typechamp[-3]:
            self.ui.labeldate1.hide()
            self.ui.calendrier1.hide()
            self.ui.labeldate2.hide()
            self.ui.calendrier2.hide()
            self.ui.deletedate2.hide()
            self.ui.labeldec2.move(self.ui.labeldec2.x(),self.ui.labeldec2.y()-180)
            self.ui.dec2.move(self.ui.dec2.x(),self.ui.dec2.y()-180)
            self.ui.labeldec1.move(self.ui.labeldec1.x(),self.ui.labeldec1.y()-180)
            self.ui.dec1.move(self.ui.dec1.x(),self.ui.dec1.y()-180)
            self.ui.deletedec.move(self.ui.deletedec.x(),self.ui.deletedec.y()-180)
            self.ui.labelra2.move(self.ui.labelra2.x(),self.ui.labelra2.y()-180)
            self.ui.ra2.move(self.ui.ra2.x(),self.ui.ra2.y()-180)
            self.ui.labelra1.move(self.ui.labelra1.x(),self.ui.labelra1.y()-180)
            self.ui.ra1.move(self.ui.ra1.x(),self.ui.ra1.y()-180)
            self.ui.deletera.move(self.ui.deletera.x(),self.ui.deletera.y()-180)
        self.ui.pushButton.move(self.ui.pushButton.x(),self.ui.pushButton.y()-180)
        self.ui.comboBox.move(self.ui.comboBox.x(),self.ui.comboBox.y()-180)
        self.typechamp.remove("IT")
        st = ""
        for i in range(0,len(self.typechamp)):
            st+= self.typechamp[i]
        self.ui.labelError.setText(st)
        
    def delRa(self):
        self.ui.comboBox.show()
        self.ui.pushButton.show()
        self.ui.comboBox.addItem("Intervalle de RA")
        self.ui.labelra1.hide()
        self.ui.ra1.hide()
        self.ui.labelra2.hide()
        self.ui.ra2.hide()
        self.ui.deletera.hide()
        if "RA" == self.typechamp[-1]:
            print(0)
        elif "RA" == self.typechamp[-2]:
            if self.typechamp[-1] == "Dec":
                self.ui.labeldec2.move(self.ui.labeldec2.x(),self.ui.labeldec2.y()-100)
                self.ui.dec2.move(self.ui.dec2.x(),self.ui.dec2.y()-100)
                self.ui.labeldec1.move(self.ui.labeldec1.x(),self.ui.labeldec1.y()-100)
                self.ui.dec1.move(self.ui.dec1.x(),self.ui.dec1.y()-100)
                self.ui.deletedec.move(self.ui.deletedec.x(),self.ui.deletedec.y()-100)
            elif self.typechamp[-1] == "DE":
                self.ui.labeldate.move(self.ui.labeldate.x(),self.ui.labeldate.y()-100)
                self.ui.calendrier.move(self.ui.calendrier.x(),self.ui.calendrier.y()-100)
                self.ui.deletedate.move(self.ui.deletedate.x(),self.ui.deletedate.y()-100)
            elif self.typechamp[-1] == "IT":
                self.ui.labeldate1.move(self.ui.labeldate1.x(),self.ui.labeldate1.y()-100)
                self.ui.calendrier1.move(self.ui.calendrier1.x(),self.ui.calendrier1.y()-100)
                self.ui.deletedate.move(self.ui.deletedate2.x(),self.ui.deletedate2.y()-100)
                self.ui.labeldate1.move(self.ui.labeldate2.x(),self.ui.labeldate2.y()-100)
                self.ui.calendrier1.move(self.ui.calendrier2.x(),self.ui.calendrier2.y()-100)
        elif "RA" == self.typechamp[-3]:
            if "IT" == self.typechamp[-1] or "IT" == self.typechamp[-2] or "IT" == self.typechamp[-3]:
                self.ui.labeldate1.move(self.ui.labeldate1.x(),self.ui.labeldate1.y()-100)
                self.ui.calendrier1.move(self.ui.calendrier1.x(),self.ui.calendrier1.y()-100)
                self.ui.deletedate2.move(self.ui.deletedate2.x(),self.ui.deletedate2.y()-100)
                self.ui.labeldate2.move(self.ui.labeldate2.x(),self.ui.labeldate2.y()-100)
                self.ui.calendrier2.move(self.ui.calendrier2.x(),self.ui.calendrier2.y()-100)
            else:
                self.ui.labeldate.move(self.ui.labeldate.x(),self.ui.labeldate.y()-100)
                self.ui.calendrier.move(self.ui.calendrier.x(),self.ui.calendrier.y()-100)
                self.ui.deletedate.move(self.ui.deletedate.x(),self.ui.deletedate.y()-100)
            self.ui.labeldec2.move(self.ui.labeldec2.x(),self.ui.labeldec2.y()-100)
            self.ui.dec2.move(self.ui.dec2.x(),self.ui.dec2.y()-100)
            self.ui.labeldec1.move(self.ui.labeldec1.x(),self.ui.labeldec1.y()-100)
            self.ui.dec1.move(self.ui.dec1.x(),self.ui.dec1.y()-100)
            self.ui.deletedec.move(self.ui.deletedec.x(),self.ui.deletedec.y()-100)
        self.ui.pushButton.move(self.ui.pushButton.x(),self.ui.pushButton.y()-100)
        self.ui.comboBox.move(self.ui.comboBox.x(),self.ui.comboBox.y()-100)
        self.typechamp.remove("RA")
        st = ""
        for i in range(0,len(self.typechamp)):
            st+= self.typechamp[i]
        self.ui.labelError.setText(st)    
    def delDec(self):
        self.ui.comboBox.show()
        self.ui.pushButton.show()
        self.ui.comboBox.addItem("Intervalle de Dec")
        self.ui.labeldec1.hide()
        self.ui.dec1.hide()
        self.ui.labeldec2.hide()
        self.ui.dec2.hide()
        self.ui.deletedec.hide()
        if "Dec" == self.typechamp[-1]:
            print(0)
        elif "Dec" == self.typechamp[-2]:
            if self.typechamp[-1] == "RA":
                self.ui.labelra2.move(self.ui.labelra2.x(),self.ui.labelra2.y()-100)
                self.ui.ra2.move(self.ui.ra2.x(),self.ui.ra2.y()-100)
                self.ui.labelra1.move(self.ui.labelra1.x(),self.ui.labelra1.y()-100)
                self.ui.ra1.move(self.ui.ra1.x(),self.ui.ra1.y()-100)
                self.ui.deletera.move(self.ui.deletera.x(),self.ui.deletera.y()-100)
            elif self.typechamp[-1] == "DE":
                self.ui.labeldate.move(self.ui.labeldate.x(),self.ui.labeldate.y()-100)
                self.ui.calendrier.move(self.ui.calendrier.x(),self.ui.calendrier.y()-100)
                self.ui.deletedate.move(self.ui.deletedate.x(),self.ui.deletedate.y()-100)
            elif self.typechamp[-1] == "IT":
                self.ui.labeldate1.move(self.ui.labeldate1.x(),self.ui.labeldate1.y()-100)
                self.ui.calendrier1.move(self.ui.calendrier1.x(),self.ui.calendrier1.y()-100)
                self.ui.deletedate2.move(self.ui.deletedate2.x(),self.ui.deletedate2.y()-100)
                self.ui.labeldate1.move(self.ui.labeldate2.x(),self.ui.labeldate2.y()-100)
                self.ui.calendrier1.move(self.ui.calendrier2.x(),self.ui.calendrier2.y()-100)
        elif "Dec" == self.typechamp[-3]:
            if "IT" == self.typechamp[-1] or "IT" == self.typechamp[-2] or "IT" == self.typechamp[-3]:
                self.ui.labeldate1.move(self.ui.labeldate1.x(),self.ui.labeldate1.y()-100)
                self.ui.calendrier1.move(self.ui.calendrier1.x(),self.ui.calendrier1.y()-100)
                self.ui.deletedate2.move(self.ui.deletedate2.x(),self.ui.deletedate2.y()-100)
                self.ui.labeldate2.move(self.ui.labeldate2.x(),self.ui.labeldate2.y()-100)
                self.ui.calendrier2.move(self.ui.calendrier2.x(),self.ui.calendrier2.y()-100)
            else:
                self.ui.labeldate.move(self.ui.labeldate.x(),self.ui.labeldate.y()-100)
                self.ui.calendrier.move(self.ui.calendrier.x(),self.ui.calendrier.y()-100)
                self.ui.deletedate.move(self.ui.deletedate.x(),self.ui.deletedate.y()-100)
            self.ui.labeldec2.move(self.ui.labeldec2.x(),self.ui.labeldec2.y()-100)
            self.ui.dec2.move(self.ui.dec2.x(),self.ui.dec2.y()-100)
            self.ui.labeldec1.move(self.ui.labeldec1.x(),self.ui.labeldec1.y()-100)
            self.ui.dec1.move(self.ui.dec1.x(),self.ui.dec1.y()-100)
            self.ui.deletedec.move(self.ui.deletedec.x(),self.ui.deletedec.y()-100)
        self.ui.pushButton.move(self.ui.pushButton.x(),self.ui.pushButton.y()-100)
        self.ui.comboBox.move(self.ui.comboBox.x(),self.ui.comboBox.y()-100)
        self.typechamp.remove("Dec")
        st = ""
        for i in range(0,len(self.typechamp)):
            st+= self.typechamp[i]
        self.ui.labelError.setText(st)
    
    def action_bouton(self):
        self.ui.labelError.setText("Recherche...")
        opra = []
        critra = []
        opdec = []
        critdec=[]
        p = Plotgraph()
        self.ui.labelError.setText("Recherche en cours")
        p.index =[]
        p.crittime = []
        p.optime = []
        if self.ui.checkdpdextcalibratedframe.isChecked():
            p.index.append("dpdextcalibratedframe")
        if self.ui.checkdpdextsingleepochframe.isChecked():
            p.index.append("dpdextsingleepochframe")
        if self.ui.checkdpdextstackedframe.isChecked():
            p.index.append("dpdextstackedframe")
        if self.ui.checkdpdmerfinalcatalog.isChecked():
            p.index.append("dpdmerfinalcatalog")
        if self.ui.checkdpdmermosaic.isChecked():
            p.index.append("dpdmermosaic")
        if self.ui.checkdpdmersegmentationmap.isChecked():
            p.index.append("dpdmersegmentationmap")
        if self.ui.checkdpdmertile.isChecked():
            p.index.append("dpdmertile")
        if self.ui.checkdpdnircalibratedframe.isChecked():
            p.index.append("dpdnircalibratedframe")
        if self.ui.checkdpdnirstackedframe.isChecked():
            p.index.append("dpdnirstackedframe")
        if self.ui.checkdpdnisprawframe.isChecked():
            p.index.append("dpdnisprawframe")
        if self.ui.checkdpdsircombinedspectra.isChecked():
            p.index.append("dpdsircombinedspectra")
        if self.ui.checkdpdspepfoutputcatalog.isChecked():
            p.index.append("dpdspepfoutputcatalog")
        if self.ui.checkdpdtrueuniverseoutput.isChecked():
            p.index.append("dpdtrueuniverseoutput")
        if self.ui.checkdpdviscalibratedframe.isChecked():
            p.index.append("dpdviscalibratedframe")
        if self.ui.checkdpdvismasterbiasframe.isChecked():
            p.index.append("dpdvismasterbiasframe")
        if self.ui.checkdpdvisstackedframe.isChecked():
            p.index.append("dpdvisstackedframe")
        if self.ui.checkdpdvisstackedframecatalog.isChecked():
            p.index.append("dpdvisstackedframecatalog")
        if self.ui.checkdpdphzpfoutputcatalog.isChecked():
            p.index.append("dpdphzpfoutputcatalog")
        
        for s in self.typechamp:
            if s=="IT":
                print("it")
                day = self.ui.calendrier1.selectedDate().day()
                if day<10 :
                    day = "0"+ str(day)
                month = self.ui.calendrier1.selectedDate().month()
                if month<10 :
                    month = "0"+ str(month)
                year = self.ui.calendrier1.selectedDate().year()
                date = str(year) + "-" + str(month) + "-" + str(day)
                p.crittime.append(date)
                p.optime.append("gte")
                
                day = self.ui.calendrier2.selectedDate().day()
                if day<10 :
                    day = "0"+ str(day)
                month = self.ui.calendrier2.selectedDate().month()
                if month<10 :
                    month = "0"+ str(month)
                year = self.ui.calendrier2.selectedDate().year()
                date = str(year) + "-" + str(month) + "-" + str(day)
                p.crittime.append(date)
                p.optime.append("lte")
            elif s == "DE":
                day = self.ui.calendrier.selectedDate().day()
                if day<10 :
                    day = "0"+ str(day)
                month = self.ui.calendrier.selectedDate().month()
                if month<10 :
                    month = "0"+ str(month)
                year = self.ui.calendrier.selectedDate().year()
                date = str(year) + "-" + str(month) + "-" + str(day)
                p.crittime.append(date)
                p.optime.append("gte")
                p.crittime.append(date)
                p.optime.append("lte")
            elif s == "Dec":
                try:
                    decmin = float(self.ui.dec1.text())
                except :
                    sys.exit("decmin n'a pas le bon format")
                try:
                    decmax = float(self.ui.dec2.text())
                except:
                    sys.exit("decmax n'a pas le bon format")
                critdec.append(decmin)
                critdec.append(decmax)
                opdec.append("gte")
                opdec.append("lte")
                
            elif s == "RA":
                try:
                    ramin=float(self.ui.ra1.text())
                except:
                    sys.exit("Ramin n'a pas le bon format")
                try :
                    ramax =float(self.ui.ra2.text())
                except:
                    sys.exit("Ramax n'a pas le bon format")
                critra.append(ramin)
                critra.append(ramax)
                opra.append("gte")
                opra.append("lte")      
        if len(p.index) == 0:
            self.ui.labelError.setText("Veuillez selectionner au moins un type d'objet dans lequel chercher")
        elif len(self.typechamp)==0:
            self.ui.labelError.setText("Veuillez selectionner une requete avant de valider")
        else:
            try:
                print(len(p.index))
                s = p.recherchespatialedateplot(index = p.index, opra = opra,critra =critra, opdec =opdec, critdec=critdec, optime = p.optime, crittime=p.crittime)
                self.ui.labelError.setText(s)
            except ConnectionError:
                self.ui.labelError.setText("Aucune base de donnée Elastic n'est connectée. Connectez-vous puis recommencez")

    def validerindex(self):
        self.ui.groupBox.setDisabled(self.ui.groupBox.isEnabled())
        self.ui.groupBox_2.setEnabled(not self.ui.groupBox.isEnabled())
        if self.ui.pushButton_2.text()=="Validate Product Types":
            self.ui.pushButton_2.setText("Modify Product Types")
        else :
            self.ui.pushButton_2.setText("Validate Product Types")
            
            
class Plotgraph(FigureCanvas):     
    coordx = []
    coordy = []
    crittime = []
    optime = []
    index = []
    dict = {}
    lines = {}
    fig = 0
    ax = 0
    t = []
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        
                
    def onclick(self,event):
        if os.path.exists("/home/user/Documents/ResultatRecherche/"):
            shutil.rmtree("/home/user/Documents/ResultatRecherche/")
        if (len(self.coordx) ==2 and len(self.coordy)==2):
            print("=2")
            print("selection d'un rectangle")
            minx = min(self.coordx)
            miny = min(self.coordy)
            maxx = max(self.coordx)
            maxy = max(self.coordy)
            print("de coordonnées ",minx,miny, maxx, maxy)
            print("recherche en cours...")
            RecordDB.filterbytime_footprint(self.index, ["lte","gte"], [maxx,minx], ["lte","gte"], [maxy,miny], self.optime, self.crittime)
            
    
        elif (len(self.coordx) ==1 and len(self.coordy)==1):
            print("=1")
            print ("selection d'un point")
            minx = self.coordx[0]
            miny = self.coordy[0]
            maxx = self.coordx[0]
            maxy = self.coordy[0]
            print("recherche en cours...")
            RecordDB.filterbytime_footprint(self.index, ["lte","gte"], [maxx,minx], ["lte","gte"], [maxy,miny], self.optime, self.crittime)
        elif (len(self.coordx) >2 and len(self.coordy)>2 and len(self.coordx)==len(self.coordy)):
            i = 0
            while i <=len(self.coordx)-2:
                if self.coordx[i]<self.coordx[i+1]:
                    maxx = self.coordx[i+1]
                    minx = self.coordx[i]
                    maxy = self.coordy[i+1]
                    miny = self.coordy[i]
                else : 
                    maxx = self.coordx[i]
                    minx = self.coordx[i+1]
                    maxy = self.coordy[i]
                    miny = self.coordy[i+1]
                RecordDB.filterbytime_footprint(self.index, ["lte","gte"], [maxx,minx], ["lte","gte"], [maxy,miny], self.optime, self.crittime)
                i+=2
        else :
            print( "Veuillez selectionner au moins un point avant de valider")
        self.coordx.clear()
        self.coordy.clear()

    def clicked(self, event):
        print(event.key)
        if event.key=='control':
            self.coordx.append(event.xdata+0.2)
            self.coordy.append(event.ydata+0.2)
            self.coordx.append(event.xdata-0.2)
            self.coordy.append(event.ydata-0.2)
        elif event.key =='shift':
            self.coordx.clear()
            self.coordy.clear()
            self.coordx.append(event.xdata)
            self.coordy.append(event.ydata)
        else :
            try:
                if 0<event.xdata<1 and 0<event.ydata<1:
                    print(event.xdata,event.ydata)
                    print(self.coordx)
                    print(self.coordy)
                    self.onclick(event)
                    self.coordx.clear()
                    self.coordy.clear()
                    
                else:
                    self.coordx.clear()
                    self.coordy.clear()
                    self.coordx.append(event.xdata)
                    self.coordy.append(event.ydata)
            except TypeError:
                sys.exit("Focus please")
            
            
    def released(self,event):
        if event.key == 'shift' :
            self.coordx.append(event.xdata)
            self.coordy.append(event.ydata) 
            
            
    def on_plot_hover(self, event):
        cpt = 0
        lis = {}
        l = []
        for txt in self.ax.texts:
            txt.set_visible(False)
        try :
            x = round(event.xdata,1)
            
            y = round(event.ydata,1)
        
            for points in self.lines :
                if x in self.lines[points][0] and y in self.lines[points][1]:
                    print (1)
                    if points not in lis:
                        print (2)
                        cpt +=1
                        lis[points] = [x,y]
                        print ("lis : ",lis)
            for i in range (0,cpt):
                l.append(i)
                print(2)
            for points in lis:
                print("points : ",points)
                print(self.ax.text(lis[points][0],lis[points][1]-0.5*l[0], points, fontsize=6))
                self.fig.canvas.draw()
                del (l[0])
            
        except TypeError:
            pass
        
    def recherchespatialeplot(self, index=[], opra = [],critra = [],opdec=[],critdec=[]):
        """
        @index est la liste comportant les index sur lesquels vont porter la recherche
        @opdec est la liste contenant les opérateurs de comparaison portant sur la declination recherché (lt,lte,gt,gte)
        @critdec est la liste des filtres définissant la declination recherché
        @opra est la liste contenant les opérateurs de comparaison portant sur la Range Ascencion recherché (lt,lte,gt,gte)
        @critra est la liste des filtres définissant la Range Ascencion recherché"""
        es = Elasticsearch()
        escat = CatClient(es)
        path1=""
        path2=""
        restotal = 0
        if len(opra)==0 and len (critra)==0:
            return self.recherchedec(index,opdec,critdec)
        elif len(opdec)==0 and len(critdec)==0:
            return self.recherchera(index,opra,critra)
        self.fig,self.ax = plt.subplots()
        self.fig.subplots_adjust(bottom=0.3)
        if (index=="*" or index=="_all"): 
            index = escat.indices(index = "_all", h="index").split("\n")
            del(index[-1])
        for ind in index:
            coordonneesx = []
            namespace = RecordDB.getnamespacefromindex(ind)
            if ind in ("dpdspepfoutputcatalog", "dpdvisstackedframecatalog", "dpdmerfinalcatalog", "dpdphzpfoutputcatalog"):
                path1 = ".Data.SpatialCoverage.Polygon.Vertex.Position.C1"
                path2 = ".Data.SpatialCoverage.Polygon.Vertex.Position.C2"
            elif ind =="dpdmertile":
                path1 = ".Data.InnerSpatialFootprint.Polygon.Vertex.Position.C1"
                path2 = ".Data.InnerSpatialFootprint.Polygon.Vertex.Position.C2"
            elif "trueuniverseoutput" in ind :
                path1 = '.Data.Pointing.RA'
                path2 = '.Data.Pointing.Dec'
            else :
                path1 = ".Data.ImgSpatialFootprint.Polygon.Vertex.Position.C1"
                path2 = ".Data.ImgSpatialFootprint.Polygon.Vertex.Position.C2"
            l = path1.split(".")
            if (len(opra)==2 and len(critra)==2 and len(opdec)==2 and len(critdec)==2):
                res_spa = es.search(index=ind, size = 10000, body= {"query" : { "bool" : { "must" :
                                                                                         [
                                                                                             {"range" : { namespace+path1:{opra[0]: critra[0] , opra[1] :critra[1]}}},
                                                                                             {"range" : { namespace+path2:{opdec[0] : critdec[0], opdec[1] : critdec[1]}}}
                                                                                            ]}}})
            elif (len(opra)==2 and len(critra)==2 and len(opdec)==1 and len(critdec)==1):
                res_spa = es.search(index=ind,_search = [namespace+path1,namespace+path2],size = 10000, body= {"query" : { "bool" : { "must" :
                                                                                         [
                                                                                            { "range" : { namespace+path1:{opra[0]: critra[0] , opra[1] :critra[1]}}},
                                                                                            {"range" : { namespace+path2:{opdec[0] : critdec[0]}}}
                                                                                            ] }}})
            elif (len(opra)==1 and len(critra)==1 and len(opdec)==1 and len(critdec)==1):
                res_spa = es.search(index=ind,size = 10000, body= {"query" : { "bool" : { "must" :
                                                                                         [
                                                                                            { "range" : { namespace+path1:{opra[0]: critra[0]}}},
                                                                                            {"range" : {namespace+path2:{opdec[0] : critdec[0]}}}
                                                                                            ] }}})
            elif (len(opra)==1 and len(critra)==1 and len(opdec)==2 and len(critdec)==2):
                res_spa = es.search(index=ind,_source = [namespace+path1,namespace+path2],size = 10000, body= {"query" : { "bool" : { "must" :
                                                                                         [
                                                                                            { "range" : { namespace+path1:{opra[0]: critra[0]}}},
                                                                                            {"range" : { namespace+path2:{opdec[0] : critdec[0], opdec[1] : critdec[1]}}}] }}})
            else:
                return("probleme avec le nombre d'arguments")
                
            restotal += res_spa['hits']['total']['value']
            for hit in res_spa['hits']['hits']:
                coordonneesx = []
                coordonneesy = []
                
                if ind == "dpdtrueuniverseoutput":
                    coordonneesx.append(hit["_source"][namespace]['Data']['Pointing']['RA'])
                    coordonneesy.append(hit["_source"][namespace]['Data']['Pointing']['Dec'])
                    line = self.ax.scatter(coordonneesx,coordonneesy,linewidths=0.5, color = colorindex(ind)) 
                    productid =hit["_source"][namespace]['Header']["ProductId"]
                    self.lines[productid] = [coordonneesx[0], coordonneesy[0]]
                else :
                    for k in range (0,4): 
                        prodx = hit["_source"][namespace]['Data'][l[2]]['Polygon']["Vertex"][k]["Position"]["C1"]
                        prody = hit["_source"][namespace]['Data'][l[2]]['Polygon']["Vertex"][k]["Position"]["C2"]
                        coordonneesx.append(prodx)
                        coordonneesy.append(prody)
                    coordonneesx.append(coordonneesx[0])
                    coordonneesy.append(coordonneesy[0])
                    productid = hit["_source"][namespace]['Header']["ProductId"]
                    line = self.ax.plot(coordonneesx,coordonneesy,color = colorindex(ind))
                    x = line[0].get_xdata()
                    y = line[0].get_ydata()
                    self.lines[productid] = [x,y]
                        
        if restotal>0:
            str2 = str(restotal) + " Résultats trouvés"
            plt.xlabel(u'Right Ascension (\N{DEGREE SIGN})', fontsize=12)
            plt.ylabel(u'Declination (\N{DEGREE SIGN})', fontsize=12)
            plt.title("Search Result", fontdict={'fontsize': 16, 'fontweight': 'bold'})
            self.fig.text(0.3, 0.05, "Press ctrl+click to select multi-points, shift+click to define an area to select click to select just a point")
            axvalider = plt.axes([0.45, 0.1, 0.1, 0.075])
            bvalider = Button(ax = axvalider, label = "Valider")
            self.mpl_connect(s='button_press_event', func=self.clicked)
            self.mpl_connect(s='button_release_event', func=self.released)
            self.fig.canvas.mpl_connect(s='motion_notify_event',func = self.on_plot_hover)
            bvalider.on_clicked(self.onclick)
            plt.show()
        else :
            str2 ="0 file matching your query"
            plt.show()
            plt.close()
        return str2
    
    
    
    def recherchespatialedateplot(self,index=[], opra = [],critra = [],opdec=[],critdec=[], optime = [], crittime = []):
        '''
        @index est la liste comportant les index sur lesquels vont orter la recherche
        @optime est la liste contenant les opérateurs de comparaison portant sur la date recherché (lt,lte,gt,gte)
        @crittime est la liste des filtres définissant la date recherché
        @opdec est la liste contenant les opérateurs de comparaison portant sur la declination recherché (lt,lte,gt,gte)
        @critdec est la liste des filtres définissant la declination recherché
        @opra est la liste contenant les opérateurs de comparaison portant sur la Range Ascencion recherché (lt,lte,gt,gte)
        @critra est la liste des filtres définissant la Range Ascencion recherché
        '''
        es = Elasticsearch()
        escat = CatClient(es)
        
        
        path1=""
        path2=""
        restotal = 0
        if len(optime)==0 and len (crittime)==0:
            if len(opra)==0 and len (critra)==0:
                return self.recherchedecplot(index,opdec=opdec,critdec=critdec)
            elif len(opdec)==0 and len(critdec)==0:
                return self.rechercheraplot(index,opra=opra,critra=critra)
            else :
                return self.recherchespatialeplot(index, opra, critra, opdec, critdec)
        if len(opdec)==0 and len(critdec)==0 and len(opra)==0 and len (critra)==0:
            return self.recherche_temporelleplot(index, optime, crittime)
        if len(opra)==0 and len (critra)==0:
            return self.recherchedectemplot(index,opdec,critdec,optime,crittime)
        if len(opdec)==0 and len(critdec)==0:
            return self.rechercheratemplot(index,opra,critra,optime,crittime)
        self.fig,self.ax = plt.subplots()
        self.fig.subplots_adjust(bottom=0.3)
        if (index=="*" or index=="_all"): 
            index = escat.indices(index = "_all", h="index").split("\n")
            del(index[-1])
        for ind in index:
            coordonneesx = []
            namespace = RecordDB.getnamespacefromindex(ind)
            if ind in ("dpdspepfoutputcatalog", "dpdvisstackedframecatalog", "dpdmerfinalcatalog", "dpdphzpfoutputcatalog"):
                path1 = ".Data.SpatialCoverage.Polygon.Vertex.Position.C1"
                path2 = ".Data.SpatialCoverage.Polygon.Vertex.Position.C2"
            elif ind =="dpdmertile":
                path1 = ".Data.InnerSpatialFootprint.Polygon.Vertex.Position.C1"
                path2 = ".Data.InnerSpatialFootprint.Polygon.Vertex.Position.C2"
            elif "dpdtrueuniverseoutput" in ind :
                path1 = '.Data.Pointing.RA'
                path2 = '.Data.Pointing.Dec'
            else :
                path1 = ".Data.ImgSpatialFootprint.Polygon.Vertex.Position.C1"
                path2 = ".Data.ImgSpatialFootprint.Polygon.Vertex.Position.C2"
            
            if ind in ("dpdnircalibratedframe", "dpdvisstackedframecatalog", "dpdviscalibratedframe"):
                date = ".Data.ObservationDateTime.UTC"
            elif ind in ("dpdextsingleepochframe", "dpdextcalibratedframe"):
                date = ".Data.ObservationDateTime.UTCObservationDateTime"
            elif "dpdtrueuniverseoutput" in ind :
                date = ".Data.ObservationDate"
            else:
                date = "DateRange.TimestampStart"
            l = path1.split(".")
            
            
            if (ind in ("dpdsircombinedspectra", "dpdmersegmentationmap", "dpdmerfinalcatalog", "dpdmermosaic", "dpdphzpfoutputcatalog", "dpdextstackedframe", "dpdmertile")) :
                continue
            elif (len(opra)==2 and len(critra)==2 and len(opdec)==1 and len(critdec)==1 and len(optime)==2 and len(crittime)==2):
                spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                           [
                                                                                               {"range": { namespace+ path1 :  {opra[0] : critra[0], opra[1] : critra[1]}}},
                                                                                               {"range": { namespace+ path2 : {opdec[0] : critdec[0]}}},
                                                                                               {"range": { namespace+ date :{optime[0] : crittime[0], optime[1] : crittime[1]}}}
                                                                                            ]    }}})
            
            elif (len(opra)==1 and len(critra)==1 and len(opdec)==1 and len(critdec)==1 and len(optime)==2 and len(crittime)==2):
                spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                           [
                                                                                               {"range": { namespace+ path1 :  {opra[0] : critra[0]}}},
                                                                                               {"range": { namespace+ path2 : {opdec[0] : critdec[0]}}},
                                                                                               {"range": { namespace+ date:{optime[0] : crittime[0], optime[1] : crittime[1]}}}
                                                                                            ]    }}})
            
            elif (len(opra)==2 and len(critra)==2 and len(opdec)==2 and len(critdec)==2 and len(optime)==2 and len(crittime)==2):
                spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                [
                                                                                    {"range": { namespace+ path1 :  {opra[0] : critra[0], opra[1] : critra[1]}}},
                                                                                    {"range": { namespace+ path2 : {opdec[0] : critdec[0], opdec[1] : critdec[1]}}},
                                                                                    {"range": { namespace+ date  :{optime[0] : crittime[0], optime[1] : crittime[1]}}}
                                                                                ]    }}})
            
            elif (len(opra)==1 and len(critra)==1 and len(opdec)==1 and len(critdec)==1 and len(optime)==1 and len(crittime)==1):
                spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                [
                                                                                    {"range": { namespace+ path1 :  {opra[0] : critra[0]}}},
                                                                                    {"range": { namespace+ path2 : {opdec[0] : critdec[0]}}},
                                                                                    {"range": { namespace+ date:{optime[0] : crittime[0]}}}
                                                                                ]    }}})            
            elif (len(opra)==2 and len(critra)==2 and len(opdec)==1 and len(critdec)==1 and len(optime)==1 and len(crittime)==1):
                spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                           [
                                                                                               {"range": { namespace + path1 :  {opra[0] : critra[0], opra[1] : critra[1]}}},
                                                                                               {"range": { namespace + path2 : {opdec[0] : critdec[0]}}},
                                                                                               {"range": { namespace + date : {optime[0] : crittime[0]}}}
                                                                                            ]    }}})
            elif (len(opra)==1 and len(critra)==1 and len(opdec)==2 and len(critdec)==2 and len(optime)==2 and len(crittime)==2):
                spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                           [
                                                                                               {"range": { namespace+path1 :  {opra[0] : critra[0]}}},
                                                                                               {"range": { namespace+path2 : {opdec[0] : critdec[0],opdec[1] : critdec[1]}}},
                                                                                               {"range": { namespace+ date :{optime[0] : crittime[0]}}}
                                                                                            ]    }}})
                
            else:
                return("probleme avec le nombre d'arguments")
            
            restotal+= spatemp['hits']['total']['value']
            
            for hit in spatemp['hits']['hits']:
                coordonneesx = []
                coordonneesy = []
                if ind == "dpdtrueuniverseoutput":
                        coordonneesx.append(hit["_source"][namespace]['Data']['Pointing']['RA'])
                        coordonneesy.append(hit["_source"][namespace]['Data']['Pointing']['Dec'])
                        self.ax.scatter(coordonneesx,coordonneesy,linewidths=0.5,color = colorindex(ind))
                        pid = hit["_source"][namespace]['Header']['Product_Id']
                        self.dict[pid] = coordonneesx,coordonneesy
                else :
                        if ind not in ("dpdsircombinedspectra", "dpdmersegmentationmap", "dpdmerfinalcatalog", "dpdmermosaic", "dpdphzpfoutputcatalog", "dpdextstackedframe", "dpdmertile"):
                            for k in range (0,4): 
                                prodx = hit["_source"][namespace]['Data'][l[2]]['Polygon']["Vertex"][k]["Position"]["C1"]
                                prody = hit["_source"][namespace]['Data'][l[2]]['Polygon']["Vertex"][k]["Position"]["C2"]
                                coordonneesx.append(prodx)
                                coordonneesy.append(prody)
                            coordonneesx.append(coordonneesx[0])
                            coordonneesy.append(coordonneesy[0])
                            productid = hit["_source"][namespace]['Header']["ProductId"]
                            line = self.ax.plot(coordonneesx,coordonneesy,color = colorindex(ind))
                            x = line[0].get_xdata()
                            y = line[0].get_ydata()
                            self.lines[productid] = [x,y]
        if restotal>0:
            
            plt.xlabel(u'Right Ascension (\N{DEGREE SIGN})', fontsize=12)
            plt.ylabel(u'Declination (\N{DEGREE SIGN})', fontsize=12)
            plt.title("Search Result", fontdict={'fontsize': 16, 'fontweight': 'bold'})
            self.fig.text(0.3, 0.05, "Press ctrl+click to select multi-points, shift+click to define an area to select click to select just a point")
            axvalider = plt.axes([0.45, 0.1, 0.1, 0.075])
            bvalider = Button(ax = axvalider, label = "Valider")
            self.fig.canvas.mpl_connect(s='button_press_event', func=self.clicked)
            self.fig.canvas.mpl_connect(s='button_release_event', func=self.released)
            self.fig.canvas.mpl_connect(s='motion_notify_event', func = self.hover)
            bvalider.on_clicked(self.onclick)
            plt.show()
        else :
            
            plt.show()
            plt.close()
        return str(restotal) + " Résultats trouvés"
    
    
    def recherchedecplot(self, index = [], critdec = [], opdec = []):
        es = Elasticsearch()
        escat = CatClient(es)
        path2=""
        restotal = 0
        self.fig,self.ax = plt.subplots()
        self.fig.subplots_adjust(bottom=0.3)
        if (index=="*" or index=="_all"): 
            index = escat.indices(index = "_all", h="index").split("\n")
            del(index[-1])
        for ind in index:
            namespace = RecordDB.getnamespacefromindex(ind)
            if ind in ("dpdspepfoutputcatalog", "dpdvisstackedframecatalog", "dpdmerfinalcatalog", "dpdphzpfoutputcatalog"):
                path2 = ".Data.SpatialCoverage.Polygon.Vertex.Position.C2"
            elif ind =="dpdmertile":
                path2 = ".Data.InnerSpatialFootprint.Polygon.Vertex.Position.C2"
            elif "trueuniverseoutput" in ind :
                path2 = '.Data.Pointing.Dec'
            else :
                path2 = ".Data.ImgSpatialFootprint.Polygon.Vertex.Position.C2"
            l = path2.split(".")
                
            if (len(opdec)==2 and len(critdec)==2):
                res_spa = es.search(index=ind,size = 10000, body= {"query" : {"range": { namespace+path2 : {opdec[0] : critdec[0],opdec[1] : critdec[1]}}}})
            elif (len(opdec)==1 and len(critdec)==1):
                res_spa = es.search(index=ind,size = 10000, body= {"query" : { "range" : { namespace+path2 : {opdec[0] : critdec[0]} }}})
            else:
                return("probleme avec le nombre d'arguments")
            
            restotal+= res_spa['hits']['total']['value']
            for hit in res_spa['hits']['hits']:
                coordonneesx = []
                coordonneesy = []
                
                if ind == "dpdtrueuniverseoutput":
                    coordonneesx.append(hit["_source"][namespace]['Data']['Pointing']['RA'])
                    coordonneesy.append(hit["_source"][namespace]['Data']['Pointing']['Dec'])
                    self.ax.scatter(coordonneesx,coordonneesy,linewidths=0.5, color = colorindex(ind))
                else :
                    for k in range (0,4): 
                        prodx = hit["_source"][namespace]['Data'][l[2]]['Polygon']["Vertex"][k]["Position"]["C1"]
                        prody = hit["_source"][namespace]['Data'][l[2]]['Polygon']["Vertex"][k]["Position"]["C2"]
                        coordonneesx.append(prodx)
                        coordonneesy.append(prody)
                    coordonneesx.append(coordonneesx[0])
                    coordonneesy.append(coordonneesy[0])
                    productid = hit["_source"][namespace]['Header']["ProductId"]
                    line = self.ax.plot(coordonneesx,coordonneesy,color = colorindex(ind))
                    x = line[0].get_xdata()
                    y = line[0].get_ydata()
                    self.lines[productid] = [x,y]    
                        
        if restotal>0:
            str2 = str(restotal) + " Résultats trouvés"
            plt.xlabel(u'Right Ascension (\N{DEGREE SIGN})', fontsize=12)
            plt.ylabel(u'Declination (\N{DEGREE SIGN})', fontsize=12)
            plt.title("Search Result", fontdict={'fontsize': 16, 'fontweight': 'bold'})
            self.fig.text(0.3, 0.05, "Press ctrl+click to select multi-points, shift+click to define an area to select click to select just a point")
            axvalider = plt.axes([0.45, 0.1, 0.1, 0.075])
            bvalider = Button(ax = axvalider, label = "Valider")
            """axreset = plt.axes([0.2, 0.05, 0.2, 0.075])
            breset = Button(ax = axreset, label = "Selectionner un sous-ensemble")"""
            self.fig.canvas.mpl_connect(s='button_press_event', func=self.clicked)
            self.fig.canvas.mpl_connect(s='button_release_event', func=self.released)
            self.fig.canvas.mpl_connect(s='motion_notify_event',func = self.on_plot_hover)
            bvalider.on_clicked(self.onclick)
            plt.show()
        else :
            str2="0 file matching your query"
            plt.show()
            plt.close()
        return str2
        
        
        
    def rechercheraplot(self,index = [],critra = [], opra = []):
        es = Elasticsearch()
        escat = CatClient(es)
        path1=""
        restotal = 0
        self.fig,self.ax = plt.subplots()
        self.fig.subplots_adjust(bottom=0.3)
        if (index=="*" or index=="_all"): 
            index = escat.indices(index = "_all", h="index").split("\n")
            del(index[-1])
        for ind in index:
            namespace = RecordDB.getnamespacefromindex(ind)
            if ind in ("dpdspepfoutputcatalog", "dpdvisstackedframecatalog", "dpdmerfinalcatalog", "dpdphzpfoutputcatalog"):
                path1 = ".Data.SpatialCoverage.Polygon.Vertex.Position.C1"
            elif ind =="dpdmertile":
                path1 = ".Data.InnerSpatialFootprint.Polygon.Vertex.Position.C1"
                # todo : faire en sorte d'utiliser 2 path (Outer)
            elif "trueuniverseoutput" in ind :
                path1 = '.Data.Pointing.RA'
            else :
                path1 = ".Data.ImgSpatialFootprint.Polygon.Vertex.Position.C1"
            l = path1.split(".")
            if (len(opra)==2 and len(critra)==2):
                res_spa = es.search(index=ind,size = 10000, body= {"query" : { "range" : { namespace+path1:{opra[0]: critra[0] , opra[1] :critra[1]}} }})
            elif (len(opra)==1 and len(critra)==1):
                res_spa = es.search(index=ind,size = 10000, body= {"query" : { "range" : { namespace+path1:{opra[0]: critra[0]}} }})
            else:
                return("probleme avec le nombre d'arguments")
            restotal+= res_spa['hits']['total']['value']
            
            for hit in res_spa['hits']['hits']:
                coordonneesx = []
                coordonneesy = []
                
                if ind == "dpdtrueuniverseoutput":
                    coordonneesx.append(hit["_source"][namespace]['Data']['Pointing']['RA'])
                    coordonneesy.append(hit["_source"][namespace]['Data']['Pointing']['Dec'])
                    self.ax.scatter(coordonneesx,coordonneesy,linewidths=0.5, color = colorindex(ind))
                else :
                    for k in range (0,4): 
                        prodx = hit["_source"][namespace]['Data'][l[2]]['Polygon']["Vertex"][k]["Position"]["C1"]
                        prody = hit["_source"][namespace]['Data'][l[2]]['Polygon']["Vertex"][k]["Position"]["C2"]
                        coordonneesx.append(prodx)
                        coordonneesy.append(prody)
                    coordonneesx.append(coordonneesx[0])
                    coordonneesy.append(coordonneesy[0])
                    productid = hit["_source"][namespace]['Header']["ProductId"]
                    line = self.ax.plot(coordonneesx,coordonneesy,color = colorindex(ind))
                    x = line[0].get_xdata()
                    y = line[0].get_ydata()
                    self.lines[productid] = [x,y]    
                        
        if restotal>0:
            #str1 = "fichiers de resultats stockes dans le dossier resultat recherche"
            str2 = str(restotal) + " Résultats trouvés"
            plt.xlabel(u'Right Ascension (\N{DEGREE SIGN})', fontsize=12)
            plt.ylabel(u'Declination (\N{DEGREE SIGN})', fontsize=12)
            plt.title("Search Result", fontdict={'fontsize': 16, 'fontweight': 'bold'})
            self.fig.text(0.3, 0.05, "Press ctrl+click to select multi-points, shift+click to define an area to select click to select just a point")
            axvalider = plt.axes([0.45, 0.1, 0.1, 0.075])
            bvalider = Button(ax = axvalider, label = "Valider")
            """axreset = plt.axes([0.2, 0.05, 0.2, 0.075])
            breset = Button(ax = axreset, label = "Selectionner un sous-ensemble")"""
            self.fig.canvas.mpl_connect(s='button_press_event', func=self.clicked)
            self.fig.canvas.mpl_connect(s='button_release_event', func=self.released)
            self.fig.canvas.mpl_connect(s='motion_notify_event',func = self.on_plot_hover)
            bvalider.on_clicked(self.onclick)
            plt.show()
        else :
            str2 ="0 file matching your query"
            plt.show()
            plt.close()
        return str2
    
    
    
    def recherche_temporelleplot(self, index, optime,crit_time):
        '''
        @index est la racine du json dans lequel rechercher
        @optime est la liste contenant les opérateurs de comparaison portant sur la date recherché
        @crit_time est la liste des filtres définissant la date recherché
        '''
        es = Elasticsearch()
        escat = CatClient(es)
        restotal = 0
        self.fig,self.ax = plt.subplots()
        self.fig.subplots_adjust(bottom=0.3)
        date=""
        if (index=="*" or index=="_all"): 
            index = escat.indices(index = "_all", h="index").split("\n")
            del(index[-1])
        for ind in index:
            namespace = RecordDB.getnamespacefromindex(ind)
            if ind in ("dpdspepfoutputcatalog", "dpdvisstackedframecatalog", "dpdmerfinalcatalog", "dpdphzpfoutputcatalog"):
                path1 = ".Data.SpatialCoverage.Polygon.Vertex.Position.C1"
            elif ind =="dpdmertile":
                path1 = ".Data.InnerSpatialFootprint.Polygon.Vertex.Position.C1"
            elif "trueuniverseoutput" in ind :
                path1 = '.Data.Pointing.RA'
                date = ".Data.ObservationDate"
            else :
                path1 = ".Data.ImgSpatialFootprint.Polygon.Vertex.Position.C1"
            if ind in ("dpdnircalibratedframe", "dpdvisstackedframecatalog", "dpdviscalibratedframe"):
                date = ".Data.ObservationDateTime.UTC"
            elif ind in ("dpdextsingleepochframe", "dpdextcalibratedframe"):
                date = ".Data.ObservationDateTime.UTCObservationDateTime"
            elif "dpdtrueuniverseoutput" in ind :
                date = ".Data.ObservationDate"
            else:
                date = "DateRange.TimestampStart"
            l=path1.split(".")
            if ind in ("dpdextstackedframe", "dpdsircombinedspectra", "dpdmersegmentationmap", "dpdmerfinalcatalog", "dpdmermosaic", "dpdphzpfoutputcatalog", "dpdmertile"):
                continue
            if (len(optime)==2 and len(crit_time)==2):
                res_temp = es.search(index=ind,size = 10000, body ={"query": { "bool" : { "must" : {"range": {namespace+date:{optime[0] : crit_time[0], optime[1] : crit_time[1]}}}}}})
                restotal += res_temp['hits']['total']['value']
            elif (len(optime)==1 and len(crit_time)==1):
                res_temp = es.search(index=ind,size = 10000, body ={"query": { "bool" : { "must" : {"range": {namespace+date:{optime[0] : crit_time[0]}}}}}})
                restotal += res_temp['hits']['total']['value']
            else :
                return("probleme avec le nombre d'arguments")
            
            
            for hit in res_temp['hits']['hits']:
                coordonneesx = []
                coordonneesy = []
                
                if ind == "dpdtrueuniverseoutput":
                    coordonneesx.append(hit["_source"][namespace]['Data']['Pointing']['RA'])
                    coordonneesy.append(hit["_source"][namespace]['Data']['Pointing']['Dec'])
                    self.ax.scatter(coordonneesx,coordonneesy,linewidths=0.5, color = colorindex(ind))
                else :
                    for k in range (0,4): 
                        prodx = hit["_source"][namespace]['Data'][l[2]]['Polygon']["Vertex"][k]["Position"]["C1"]
                        prody = hit["_source"][namespace]['Data'][l[2]]['Polygon']["Vertex"][k]["Position"]["C2"]
                        coordonneesx.append(prodx)
                        coordonneesy.append(prody)
                    coordonneesx.append(coordonneesx[0])
                    coordonneesy.append(coordonneesy[0])
                    productid = hit["_source"][namespace]['Header']["ProductId"]
                    line = self.ax.plot(coordonneesx,coordonneesy,color = colorindex(ind))
                    x = line[0].get_xdata()
                    y = line[0].get_ydata()
                    self.lines[productid] = [x,y]    
                        
        if restotal>0:
            str2 = str(restotal) + " Résultats trouvés"
            plt.xlabel(u'Right Ascension (\N{DEGREE SIGN})', fontsize=12)
            plt.ylabel(u'Declination (\N{DEGREE SIGN})', fontsize=12)
            plt.title("Search Result", fontdict={'fontsize': 16, 'fontweight': 'bold'})
            self.fig.text(0.3, 0.05, "Press ctrl+click to select multi-points, shift+click to define an area to select click to select just a point")
            axvalider = plt.axes([0.45, 0.1, 0.1, 0.075])
            bvalider = Button(ax = axvalider, label = "Valider")
            self.fig.canvas.mpl_connect(s='button_press_event', func=self.clicked)
            self.fig.canvas.mpl_connect(s='button_release_event', func=self.released)
            self.fig.canvas.mpl_connect(s='motion_notify_event',func = self.on_plot_hover)
            bvalider.on_clicked(self.onclick)
            plt.show()
        else :
            str2 = "0 file matching your query"
            plt.show()
            plt.close()
        return str2
    
    
    def rechercheratemplot(self, index,opra,critra,optime,crittime):
        es = Elasticsearch()
        escat = CatClient(es)
        restotal = 0
        self.fig,self.ax = plt.subplots()
        self.fig.subplots_adjust(bottom=0.3)
        date=""
        if (index=="*" or index=="_all"): 
            index = escat.indices(index = "_all", h="index").split("\n")
            del(index[-1])
        for ind in index:
            coordonneesx = []
            namespace = RecordDB.getnamespacefromindex(ind)
            if ind in ("dpdspepfoutputcatalog", "dpdvisstackedframecatalog", "dpdmerfinalcatalog", "dpdphzpfoutputcatalog"):
                path1 = ".Data.SpatialCoverage.Polygon.Vertex.Position.C1"
            elif ind =="dpdmertile":
                path1 = ".Data.InnerSpatialFootprint.Polygon.Vertex.Position.C1"
                # todo : faire en sorte d'utiliser 2 path (Outer)
            elif "trueuniverseoutput" in ind :
                path1 = '.Data.Pointing.RA'
                date = ".Data.ObservationDate"
            else :
                path1 = ".Data.ImgSpatialFootprint.Polygon.Vertex.Position.C1"
            
            if ind in ("dpdnircalibratedframe", "dpdvisstackedframecatalog", "dpdviscalibratedframe"):
                date = ".Data.ObservationDateTime.UTC"
            elif ind in ("dpdextsingleepochframe", "dpdextcalibratedframe"):
                date = ".Data.ObservationDAteTime.UTCObservationDateTime"
            else:
                date = "DateRange.TimestampStart"
            l = path1.split(".")
            if (len (optime)==0 and len(crittime == 0)) or (ind in ("dpdextstackedframe", "dpdsircombinedspectra", "dpdmersegmentationmap", "dpdmerfinalcatalog", "dpdmermosaic", "dpdphzpfoutputcatalog", "dpdextstackedframe", "dpdmertile", "dpdmerfinalcatalog")) :
                if (len(opra)==2 and len(critra)==2):
                    spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                               [
                                                                                                   {"range": { namespace+ path1 :  {opra[0] : critra[0], opra[1] : critra[1]}}},
                                                                                                   ]    }}})
                elif (len(opra)==1 and len(critra)==1):
                    spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                               [
                                                                                                   {"range": { namespace+ path1 :  {opra[0] : critra[0]}}},
                                                                                                ]    }}})
                else:
                    return("probleme avec le nombre d'arguments")
            elif (len(opra)==2 and len(critra)==2 and len(optime)==2 and len(crittime)==2):
                spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                           [
                                                                                               {"range": { namespace+ path1 :  {opra[0] : critra[0], opra[1] : critra[1]}}},
                                                                                               {"range": { namespace+ date :{optime[0] : crittime[0], optime[1] : crittime[1]}}}
                                                                                            ]    }}})
            
            elif (len(opra)==1 and len(critra)==1 and len(optime)==2 and len(crittime)==2):
                spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                           [
                                                                                               {"range": { namespace+ path1 :  {opra[0] : critra[0]}}},
                                                                                               {"range": { namespace+ date:{optime[0] : crittime[0], optime[1] : crittime[1]}}}
                                                                                            ]    }}})
            
            elif (len(opra)==1 and len(critra)==1 and len(optime)==1 and len(crittime)==1):
                spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                [
                                                                                    {"range": { namespace+ path1 :  {opra[0] : critra[0]}}},
                                                                                    {"range": { namespace+ date:{optime[0] : crittime[0]}}}
                                                                                ]    }}})            
            elif (len(opra)==2 and len(critra)==2 and len(optime)==1 and len(crittime)==1):
                spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                           [
                                                                                               {"range": { namespace + path1 :  {opra[0] : critra[0], opra[1] : critra[1]}}},
                                                                                               {"range": { namespace + date : {optime[0] : crittime[0]}}}
                                                                                            ]    }}})
                
            else:
                return("probleme avec le nombre d'arguments")
            restotal += spatemp['hits']['total']['value']
            for hit in spatemp['hits']['hits']:
                coordonneesx = []
                coordonneesy = []
                
                if ind == "dpdtrueuniverseoutput":
                    coordonneesx.append(hit["_source"][namespace]['Data']['Pointing']['RA'])
                    coordonneesy.append(hit["_source"][namespace]['Data']['Pointing']['Dec'])
                    self.ax.scatter(coordonneesx,coordonneesy,linewidths=0.5, color = colorindex(ind))
                else :
                    for k in range (0,4): 
                        prodx = hit["_source"][namespace]['Data'][l[2]]['Polygon']["Vertex"][k]["Position"]["C1"]
                        prody = hit["_source"][namespace]['Data'][l[2]]['Polygon']["Vertex"][k]["Position"]["C2"]
                        coordonneesx.append(prodx)
                        coordonneesy.append(prody)
                    coordonneesx.append(coordonneesx[0])
                    coordonneesy.append(coordonneesy[0])
                    productid = hit["_source"][namespace]['Header']["ProductId"]
                    line = self.ax.plot(coordonneesx,coordonneesy,color = colorindex(ind))
                    x = line[0].get_xdata()
                    y = line[0].get_ydata()
                    self.lines[productid] = [x,y]    
                        
        if restotal>0:
            #str1 = "fichiers de resultats stockes dans le dossier resultat recherche"
            str2 = str(restotal) + " Résultats trouvés"
            plt.xlabel(u'Right Ascension (\N{DEGREE SIGN})', fontsize=12)
            plt.ylabel(u'Declination (\N{DEGREE SIGN})', fontsize=12)
            plt.title("Search Result", fontdict={'fontsize': 16, 'fontweight': 'bold'})
            self.fig.text(0.3, 0.05, "Press ctrl+click to select multi-points, shift+click to define an area to select click to select just a point")
            axvalider = plt.axes([0.45, 0.1, 0.1, 0.075])
            bvalider = Button(ax = axvalider, label = "Valider")
            """axreset = plt.axes([0.2, 0.05, 0.2, 0.075])
            breset = Button(ax = axreset, label = "Selectionner un sous-ensemble")"""
            self.fig.canvas.mpl_connect(s='button_press_event', func=self.clicked)
            self.fig.canvas.mpl_connect(s='button_release_event', func=self.released)
            self.fig.canvas.mpl_connect(s='motion_notify_event',func = self.on_plot_hover)
            bvalider.on_clicked(self.onclick)
            plt.show()
        else :
            str2 = "0 file matching your query"
            plt.show()
            plt.close()
        return str2
    
    
    
    def recherchedectemplot(self, index,opdec,critdec,optime,crittime):
        es = Elasticsearch()
        escat = CatClient(es)
        restotal = 0
        self.fig,self.ax = plt.subplots()
        self.fig.subplots_adjust(bottom=0.3)
        date=""
        if (index=="*" or index=="_all"): 
            index = escat.indices(index = "_all", h="index").split("\n")
            del(index[-1])
        for ind in index:
            coordonneesx = []
            namespace = RecordDB.getnamespacefromindex(ind)
            if ind in ("dpdspepfoutputcatalog", "dpdvisstackedframecatalog", "dpdmerfinalcatalog", "dpdphzpfoutputcatalog"):
                path2 = ".Data.SpatialCoverage.Polygon.Vertex.Position.C2"
            elif ind =="dpdmertile":
                path2 = ".Data.InnerSpatialFootprint.Polygon.Vertex.Position.C2"
                # todo : faire en sorte d'utiliser 2 path (Outer)
            elif "trueuniverseoutput" in ind :
                path2 = '.Data.Pointing.Dec'
                date = ".Data.ObservationDate"
            else :
                path2 = ".Data.ImgSpatialFootprint.Polygon.Vertex.Position.C2"
            
            if ind in ("dpdnircalibratedframe", "dpdvisstackedframecatalog", "dpdviscalibratedframe"):
                date = ".Data.ObservationDateTime.UTC"
            elif ind in ("dpdextsingleepochframe", "dpdextcalibratedframe"):
                date = ".Data.ObservationDAteTime.UTCObservationDateTime"
            else:
                date = "DateRange.TimestampStart"
            l = path2.split(".")
            if (len (optime)==0 and len(crittime == 0)) or (ind in ("dpdextstackedframe", "dpdsircombinedspectra", "dpdmersegmentationmap", "dpdmerfinalcatalog", "dpdmermosaic", "dpdphzpfoutputcatalog", "dpdextstackedframe", "dpdmertile", "dpdmerfinalcatalog")) :
                if (len(opdec)==1 and len(critdec)==1):
                    spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                               [
                                                                                                   {"range": { namespace+ path2 : {opdec[0] : critdec[0]}}}
                                                                                                   ]    }}})
                elif (len(opdec)==2 and len(critdec)==2):
                    spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                               [
                                                                                                   {"range": { namespace+ path2 : {opdec[0] : critdec[0], opdec[1] : critdec[1]}}}
                                                                                                ]    }}})
                else:
                    return("probleme avec le nombre d'arguments")
            elif (len(opdec)==1 and len(critdec)==1 and len(optime)==2 and len(crittime)==2):
                spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                           [
                                                                                               {"range": { namespace+ path2 : {opdec[0] : critdec[0]}}},
                                                                                               {"range": { namespace+ date :{optime[0] : crittime[0], optime[1] : crittime[1]}}}
                                                                                            ]    }}})
            
            elif (len(opdec)==2 and len(critdec)==2 and len(optime)==2 and len(crittime)==2):
                spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                [
                                                                                    {"range": { namespace+ path2 : {opdec[0] : critdec[0], opdec[1] : critdec[1]}}},
                                                                                    {"range": { namespace+ date  :{optime[0] : crittime[0], optime[1] : crittime[1]}}}
                                                                                ]    }}})
            
            elif (len(opdec)==1 and len(critdec)==1 and len(optime)==1 and len(crittime)==1):
                spatemp = es.search(index = ind,size = 10000, body = {"query" : { "bool" : { "must" : 
                                                                                [
                                                                                    {"range": { namespace+ path2 : {opdec[0] : critdec[0]}}},
                                                                                    {"range": { namespace+ date:{optime[0] : crittime[0]}}}
                                                                                ]    }}})
                
            else:
                return("probleme avec le nombre d'arguments")
            restotal += spatemp['hits']['total']['value']
            for hit in spatemp['hits']['hits']:
                coordonneesx = []
                coordonneesy = []
                
                if ind == "dpdtrueuniverseoutput":
                    coordonneesx.append(hit["_source"][namespace]['Data']['Pointing']['RA'])
                    coordonneesy.append(hit["_source"][namespace]['Data']['Pointing']['Dec'])
                    self.ax.scatter(coordonneesx,coordonneesy,linewidths=0.5, color = colorindex(ind))
                else :
                    for k in range (0,4): 
                        prodx = hit["_source"][namespace]['Data'][l[2]]['Polygon']["Vertex"][k]["Position"]["C1"]
                        prody = hit["_source"][namespace]['Data'][l[2]]['Polygon']["Vertex"][k]["Position"]["C2"]
                        coordonneesx.append(prodx)
                        coordonneesy.append(prody)
                    coordonneesx.append(coordonneesx[0])
                    coordonneesy.append(coordonneesy[0])
                    productid = hit["_source"][namespace]['Header']["ProductId"]
                    line = self.ax.plot(coordonneesx,coordonneesy,color = colorindex(ind))
                    x = line[0].get_xdata()
                    y = line[0].get_ydata()
                    self.lines[productid] = [x,y]    
                        
        if restotal>0:
            #str1 = "fichiers de resultats stockes dans le dossier resultat recherche"
            str2 = str(restotal) + " Résultats trouvés"
            plt.xlabel(u'Right Ascension (\N{DEGREE SIGN})', fontsize=12)
            plt.ylabel(u'Declination (\N{DEGREE SIGN})', fontsize=12)
            plt.title("Search Result", fontdict={'fontsize': 16, 'fontweight': 'bold'})
            self.fig.text(0.3, 0.05, "Press ctrl+click to select multi-points, shift+click to define an area to select click to select just a point")
            axvalider = plt.axes([0.45, 0.1, 0.1, 0.075])
            bvalider = Button(ax = axvalider, label = "Valider")
            """axreset = plt.axes([0.2, 0.05, 0.2, 0.075])
            breset = Button(ax = axreset, label = "Selectionner un sous-ensemble")"""
            self.fig.canvas.mpl_connect(s='button_press_event', func=self.clicked)
            self.fig.canvas.mpl_connect(s='button_release_event', func=self.released)
            self.fig.canvas.mpl_connect(s='motion_notify_event',func = self.on_plot_hover)
            bvalider.on_clicked(self.onclick)
            plt.show()
        else:
            str2 = "0 file matching your query"
            plt.show()
            plt.close()
        return str2

            
    def matchall(self):
        return 0 ;


def window():
    app = QApplication(sys.argv)
    win = MyWindow()
    win.show()
    app.exec_()
    
    
window()