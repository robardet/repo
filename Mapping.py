'''
Created on Jun 30, 2020

@author: user
'''
import xml.etree.ElementTree as ET
import os
from matplotlib import pyplot as plt
from matplotlib.widgets import Button
from matplotlib.lines import Line2D as l
import RecordDB

def colorindex(index):
    if index == "dpdspepfoutputcatalog" :
        return "blue"
    elif index == "dpdmertile" : 
        return "orange"
    elif index == "dpdtrueuniverseoutput" :
        return "grey"
    elif index == "dpdnirstackedframe" :
        return "darkturquoise"
    elif index == "dpdnircalibratedframe" :
        return "c"
    elif index == "dpdextcalibratedframe" :
        return "chartreuse"
    elif index == "dpdvismasterbiasframe":
        return "darkviolet"
    elif index == "dpdvisstackedframe":
        return "fuchsia"
    elif index == "dpdsircombinedspectra":
        return "darkslategrey"
    elif index == "dpdvisstackedframecatalog":
        return "indigo"
    elif index == "dpdmersegmentationmap":
        return "orangered"
    elif index == "dpdnisprawframe":
        return "black"
    elif index == "dpdmerfinalcatalog":
        return "red"
    elif index == "dpdmermosaic":
        return "yellow"
    elif index == "dpdphzpfoutputcatalog":
        return "peru"
    elif index == "dpdextsingleepochframe":
        return "darkgreen"
    elif index == "dpdextstackedframe":
        return "olivedrab"
    else:
        return "crimson"
    
def ick(button_press_event):
    print( str(button_press_event.xdata), str(button_press_event.ydata))
    
def nouveaupoint(event):
    tab = []
    tab.append(int(event.xdata))
    tab.append(int(event.ydata))
    print (tab)
    return tab

def returnxml(tuple): 
    print(tuple)
    #RecordDB.filterbytime_footprint(index="*", opspacera = ["lte","gte"], critspacera = [tuple[0],tuple[0]], opspacedec=["lte","gte"], critspacedec = [tuple[1],tuple[1]])
    
def graffromjson():
    x = 0
    y = 0
    #plt.subplot(111, projection="aitoff")
    fig,ax = plt.subplots()
    """plt.xlim(0,50)
    plt.ylim(0,200.0)"""
    for subdir, dirs, files in os.walk("/home/user/Documents/ResultatRecherche/"):
        for filename in files:
            filepath = subdir + os.sep + filename
            i = subdir.split("/")
            ind = i[-1]
            if filepath.endswith(".xml") :
                if ind in ("dpdspepfoutputcatalog", "dpdvisstackedframecatalog", "dpdmerfinalcatalog", "dpdphzpfoutputcatalog"):
                    path = "./Data/SpatialCoverage/Polygon/Vertex/Position"
                elif ind =="dpdmertile" :
                    path = "./Data/InnerSpatialFootprint/Polygon/Vertex/Position"
                    # todo : faire en sorte d'utiliser 2 path (Outer)
                elif ind == "dpdtrueuniverseoutput" :
                    path = './Data/Pointing'
                elif ind =="dpdsircombinedspectra" :
                    path = "./Data/Footprint/Polygon/Vertex/Position"
                else :
                    path = "./Data/ImgSpatialFootprint/Polygon/Vertex/Position"
                ra=[]
                dec=[]
                try:
                    tree = ET.parse(filepath)
                except :
                    print(ind)
                root = tree.getroot()
                for pos in root.findall(path):
                    if ind == "dpdtrueuniverseoutput" :
                        ra.append(float(pos.find('RA').text))
                        dec.append(float(pos.find('Dec').text))
                        plt.scatter(ra,dec, color = colorindex(ind))
                    elif ind != "dpdvismasterbiasframe" :
                        ra.append(float(pos.find('C1').text))
                        dec.append(float(pos.find('C2').text))
                if ind !="dpdvismasterbiasframe":
                        ra.append(ra[0])
                        dec.append(dec[0])
                l, = plt.plot(ra, dec,color=colorindex(ind), linewidth =0.5)
    plt.xlabel(u'Right Ascension (\N{DEGREE SIGN})', fontsize=12)
    plt.ylabel(u'Declination (\N{DEGREE SIGN})', fontsize=12)
    plt.title("Resultat de la Recherche", fontdict={'fontsize': 16, 'fontweight': 'bold'})
    plt.subplots_adjust(bottom=0.3)
    axvalider = plt.axes([0.5, 0.05, 0.1, 0.075])
    bvalider = Button(ax = axvalider, label = "Valider")
    clicd = plt.ginput(n = 5, mouse_add = 1, mouse_pop = 3, show_clicks=True)
    tab = clicd
    bvalider.on_clicked(returnxml(clicd))
    plt.show()

    plt.grid(True)
def graf():
    x = 0
    y = 0
    #plt.subplot(111, projection="aitoff")
    fig,ax = plt.subplots()
    """plt.xlim(0,50)
    plt.ylim(0,200.0)"""
    for subdir, dirs, files in os.walk("/home/user/Documents/ResultatRecherche/"):
        for filename in files:
            filepath = subdir + os.sep + filename
            i = subdir.split("/")
            ind = i[-1]
            if filepath.endswith(".xml") :
                if ind in ("dpdspepfoutputcatalog", "dpdvisstackedframecatalog", "dpdmerfinalcatalog", "dpdphzpfoutputcatalog"):
                    path = "./Data/SpatialCoverage/Polygon/Vertex/Position"
                elif ind =="dpdmertile" :
                    path = "./Data/InnerSpatialFootprint/Polygon/Vertex/Position"
                    # todo : faire en sorte d'utiliser 2 path (Outer)
                elif ind == "dpdtrueuniverseoutput" :
                    path = './Data/Pointing'
                elif ind =="dpdsircombinedspectra" :
                    path = "./Data/Footprint/Polygon/Vertex/Position"
                else :
                    path = "./Data/ImgSpatialFootprint/Polygon/Vertex/Position"
                ra=[]
                dec=[]
                try:
                    tree = ET.parse(filepath)
                except :
                    print(ind)
                root = tree.getroot()
                for pos in root.findall(path):
                    if ind == "dpdtrueuniverseoutput" :
                        ra.append(float(pos.find('RA').text))
                        dec.append(float(pos.find('Dec').text))
                        plt.scatter(ra,dec, color = colorindex(ind))
                    elif ind != "dpdvismasterbiasframe" :
                        ra.append(float(pos.find('C1').text))
                        dec.append(float(pos.find('C2').text))
                if ind !="dpdvismasterbiasframe":
                        ra.append(ra[0])
                        dec.append(dec[0])
                l, = plt.plot(ra, dec,color=colorindex(ind), linewidth =0.5)
    plt.xlabel(u'Right Ascension (\N{DEGREE SIGN})', fontsize=12)
    plt.ylabel(u'Declination (\N{DEGREE SIGN})', fontsize=12)
    plt.title("Resultat de la Recherche", fontdict={'fontsize': 16, 'fontweight': 'bold'})
    plt.subplots_adjust(bottom=0.3)
    axvalider = plt.axes([0.5, 0.05, 0.1, 0.075])
    bvalider = Button(ax = axvalider, label = "Valider")
    clicd = plt.ginput(n = 5, mouse_add = 1, mouse_pop = 3, show_clicks=True)
    tab = clicd
    bvalider.on_clicked(returnxml(clicd))
    plt.show()

    plt.grid(True)


#graf()

