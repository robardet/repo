'''
Created on Jun 11, 2020

@author: user
'''
import sys
import os.path 
from elasticsearch import Elasticsearch
from elasticsearch.client import IndicesClient
import conversion
import xml.etree.ElementTree as ET
from elasticsearch.client.cat import CatClient
import glob
import shutil
from matplotlib import pyplot as plt
from matplotlib.widgets import Button
from matplotlib.lines import Line2D as l
import json


es = Elasticsearch()
es.ic= IndicesClient(es)



def listdirectory(path): 
    fichier=[] 
    for root, dirs, files in os.walk(path): 
        for i in files: 
            fichier.append(os.path.join(root, i)) 
    return fichier


def patchfiles(path):
    tree = ET.parse(path)
    root = tree.getroot()
    for node in root.findall(".//OuterMOCIndex"):
        if len(node.text) <2:
            node.text += " 0"
    for node in root.findall(".//InnerMOCIndex"):
        if len(node.text) <2:
            node.text += " 0"
    for node in root.findall(".//AxisLengths"):
        if len(node.text) <2:
            node.text += " 0"
    for node in root.findall(".//ObservationIdList"):
        if len(node.text) <2:
            node.text += " 0"
    tree.write(path)
    
    



def allnodes(root):
    """xml root"""
    for child in root :
        dict = {}
        while child:
            for descendant in child :
                dict[child.tag] = descendant.tag
                print(dict)
            








def enregistrer_donnee(path_to_directory):
    debutrecord = os.times()[0]
    arborescence=listdirectory(path_to_directory)
    dicterr ={}
    tlecture = 0
    tconversion = 0
    tingestion = 0
    n = 0
    for i in range(0,len(arborescence)):
        if ".xml" in arborescence[i] : 
            patchfiles(arborescence[i])
            '''lecture'''
            debutlecture = float(os.times()[0])
            tree = ET.parse(arborescence[i])
            root = tree.getroot()
            product_type = root[0][1].text.lower()
            product = 0
            finlecture = float(os.times()[0])
            tlecture+=(finlecture-debutlecture)
            '''conversion'''
            debutconversion = float(os.times()[0])
            if not os.path.exists("/home/user/Documents/JsonData/" + str(product_type)):
                os.makedirs("/home/user/Documents/JsonData/" + product_type)
            while os.path.exists("/home/user/Documents/JsonData/" + product_type + "/" + str(product) + ".json"):
                product += 1
            path_to_json = "/home/user/Documents/JsonData/" + product_type + "/" + str(product) + ".json"
            conversion.xmltojson(arborescence[i], path_to_json)
            finconversion = float(os.times()[0])
            tconversion += (finconversion - debutconversion)
            '''ingestion'''
            debutingestion = float(os.times()[0])
            json = open(path_to_json,"r")
            try:
                es.index(index=product_type,id = str(product), body=json.read())
            except :
                if product_type in dicterr:
                    dicterr[product_type]+=1
                else :
                    dicterr[product_type]=1
            n+=1
            finingestion = float(os.times()[0])
            tingestion += (finingestion-debutingestion)
        if n%100 == 0:
            print (n)
    print (dicterr)
    finrecord = float(os.times()[0])
    print("temps de lecture :",tlecture)
    print("temps de conversion : ",tconversion)
    print("temps d'ingestion : ", tingestion)
    print("temps total : ",finrecord-debutrecord)
    return n
    
def resetDB():
    es = Elasticsearch()
    escat = CatClient(es)
    index = escat.indices(index = "_all", h="index").split("\n")
    del(index[-1])
    for ind in index:
        es.delete_by_query(index=ind,    body={"query": {"match_all": {}}})
    if os.path.exists("/home/user/Documents/JsonData"):
        shutil.rmtree("/home/user/Documents/JsonData")
    return index


def getallindex():
    es = Elasticsearch()
    escat = CatClient(es)
    index = escat.indices(index = "_all", h="index").split("\n")
    del(index[-1])
    return index


def getAllXml(path_to_directory):
    arborescence=listdirectory(path_to_directory)
    for i in range(0,len(arborescence)):
        if ".json" in arborescence[i]:
            a=arborescence[i].rstrip(".json")
            l = a.split("/")
            name = l[-2]+ "/" + l[-1]
            if not os.path.exists("/home/user/Documents/Xml2/" + l[-2]):
                os.makedirs("/home/user/Documents/Xml2/" + l[-2] + "/")
            path_to_xml = "/home/user/Documents/Xml2/" + name + ".xml"
            conversion.jsontoxml(arborescence[i], path_to_xml)
       



def getnamespacefromindex(index) -> str:
    if index == "dpdspepfoutputcatalog" :
        return "{http://euclid.esa.org/schema/dpd/spe/spezcatalog}DpdSpePfOutputCatalog"
    elif index == "dpdmertile" : 
        return "{http://euclid.esa.org/schema/dpd/mer/tile}DpdMerTile"
    elif index == "dpdtrueuniverseoutput" :
        return "{http://euclid.esa.org/schema/dpd/sim/trueuniverseoutput}DpdTrueUniverseOutput"
    elif index == "dpdnirstackedframe" :
        return "{http://euclid.esa.org/schema/dpd/nir/stackedframe}DpdNirStackedFrame"
    elif index == "dpdnircalibratedframe" :
        return "{http://euclid.esa.org/schema/dpd/nir/calibratedframe}DpdNirCalibratedFrame"
    elif index == "dpdextcalibratedframe" :
        return "{http://euclid.esa.org/schema/dpd/ext/calibratedframe}DpdExtCalibratedFrame"
    elif index == "dpdvismasterbiasframe":
        return "{http://euclid.esa.org/schema/dpd/vis/vismasterbiasframe}DpdVisMasterBiasFrame"
    elif index == "dpdvisstackedframe":
        return "{http://euclid.esa.org/schema/dpd/vis/visstackedframe}DpdVisStackedFrame"
    elif index == "dpdsircombinedspectra":
        return "{http://euclid.esa.org/schema/dpd/sir/combinedspectra}DpdSirCombinedSpectra"
    elif index == "dpdvisstackedframecatalog":
        return "{http://euclid.esa.org/schema/dpd/vis/stackedframecatalog}DpdVisStackedFrameCatalog"
    elif index == "dpdmersegmentationmap":
        return "{http://euclid.esa.org/schema/dpd/mer/segmentationmap}DpdMerSegmentationMap"
    elif index == "dpdnisprawframe":
        return "{http://euclid.esa.org/schema/dpd/le1/nisprawframe}DpdNispRawFrame"
    elif index == "dpdmerfinalcatalog":
        return "{http://euclid.esa.org/schema/dpd/mer/finalcatalog}DpdMerFinalCatalog"
    elif index == "dpdmermosaic":
        return "{http://euclid.esa.org/schema/dpd/mer/mosaic}DpdMerMosaic"
    elif index == "dpdphzpfoutputcatalog":
        return "{http://euclid.esa.org/schema/dpd/phz/outputcatalog}DpdPhzPfOutputCatalog"
    elif index == "dpdextsingleepochframe":
        return "{http://euclid.esa.org/schema/dpd/ext/singleepochframe}DpdExtSingleEpochFrame"
    elif index == "dpdextstackedframe":
        return "{http://euclid.esa.org/schema/dpd/ext/stackedframe}DpdExtStackedFrame"
    else:
        return "{http://euclid.esa.org/schema/dpd/vis/calibratedframe}DpdVisCalibratedFrame"

    
def filterbytime_footprint(index, opspacera = [],critspacera = [],opspacedec = [], critspacedec = [], optime = [],crit_time = []):
    es = Elasticsearch()
    escat = CatClient(es)
    '''
    @index est l'index dans lequel rechercher
    @optime est la liste contenant les opérateurs de comparaison portant sur la date recherché
    @crit_time est la liste des filtres définissant la date recherché
    @opspacera est la liste contenant les opérateurs de comparaison portant sur l'endroit recherché
    @critspacera est la liste des filtres définissant lendroit recherché
    @crit_foot est la liste des filtres définissant lendroit recherché
    '''
    if (len(opspacedec)==0and len(critspacedec)==0 and len(opspacera)==0 and len(critspacera)==0):
        return recherche_temporelle(index, optime,crit_time)
    elif (len(optime)==0and len(crit_time)==0):
        return recherchespatiale(index, opspacera, critspacera, opspacedec, critspacedec)
    path1=""
    path2=""
    restotal = 0
    if (index=="*" or index=="_all" or index == ""): 
        index = escat.indices(index = "_all", h="index").split("\n")
        del(index[-1])
    for ind in index:
        product = []
        namespace = getnamespacefromindex(ind)
        if ind in ("dpdspepfoutputcatalog", "dpdvisstackedframecatalog", "dpdmerfinalcatalog", "dpdphzpfoutputcatalog"):
            path1 = ".Data.SpatialCoverage.Polygon.Vertex.Position.C1"
            path2 = ".Data.SpatialCoverage.Polygon.Vertex.Position.C2"
        elif ind =="dpdmertile":
            path1 = ".Data.InnerSpatialFootprint.Polygon.Vertex.Position.C1"
            path2 = ".Data.InnerSpatialFootprint.Polygon.Vertex.Position.C2"
            # todo : faire en sorte d'utiliser 2 path (Outer)
        elif "dpdtrueuniverseoutput" in ind :
            path1 = '.Data.Pointing.RA'
            path2 = '.Data.Pointing.Dec'
            date = ".Data.ObservationDate"
        else :
            path1 = ".Data.ImgSpatialFootprint.Polygon.Vertex.Position.C1"
            path2 = ".Data.ImgSpatialFootprint.Polygon.Vertex.Position.C2"
        
        if ind in ("dpdnircalibratedframe", "dpdvisstackedframecatalog", "dpdviscalibratedframe"):
            date = ".Data.ObservationDateTime.UTC"
        elif ind in ("dpdextsingleepochframe", "dpdextcalibratedframe"):
            date = ".Data.ObservationDateTime.UTCObservationDateTime"
        else:
            date = "DateRange.TimestampStart"
        
        if (ind in ("dpdsircombinedspectra", "dpdmersegmentationmap", "dpdmermosaic", "dpdphzpfoutputcatalog", "dpdextstackedframe", "dpdmertile", "dpdmerfinalcatalog")) :
            continue
        elif (len(opspacera)==2 and len(critspacera)==2 and len(opspacedec)==1 and len(critspacedec)==1 and len(optime)==2 and len(crit_time)==2):
            spatemp = es.search(index = ind,size = 1000, body = {"query" : { "bool" : { "must" : 
                                                                                       [
                                                                                           {"range": { namespace+ path1 :  {opspacera[0] : critspacera[0], opspacera[1] : critspacera[1]}}},
                                                                                           {"range": { namespace+ path2 : {opspacedec[0] : critspacedec[0]}}},
                                                                                           {"range": { namespace+ date :{optime[0] : crit_time[0], optime[1] : crit_time[1]}}}
                                                                                        ]    }}})
        
        elif (len(opspacera)==1 and len(critspacera)==1 and len(opspacedec)==1 and len(critspacedec)==1 and len(optime)==2 and len(crit_time)==2):
            spatemp = es.search(index = ind,size = 1000, body = {"query" : { "bool" : { "must" : 
                                                                                       [
                                                                                           {"range": { namespace+ path1 :  {opspacera[0] : critspacera[0]}}},
                                                                                           {"range": { namespace+ path2 : {opspacedec[0] : critspacedec[0]}}},
                                                                                           {"range": { namespace+ date:{optime[0] : crit_time[0], optime[1] : crit_time[1]}}}
                                                                                        ]    }}})
        
        elif (len(opspacera)==2 and len(critspacera)==2 and len(opspacedec)==2 and len(critspacedec)==2 and len(optime)==2 and len(crit_time)==2):
            spatemp = es.search(index = ind,size = 1000, body = {"query" : { "bool" : { "must" : 
                                                                            [
                                                                                {"range": { namespace+ path1 :  {opspacera[0] : critspacera[0], opspacera[1] : critspacera[1]}}},
                                                                                {"range": { namespace+ path2 : {opspacedec[0] : critspacedec[0], opspacedec[1] : critspacedec[1]}}},
                                                                                {"range": { namespace+ date  :{optime[0] : crit_time[0], optime[1] : crit_time[1]}}}
                                                                            ]    }}})
        
        elif (len(opspacera)==1 and len(critspacera)==1 and len(opspacedec)==1 and len(critspacedec)==1 and len(optime)==1 and len(crit_time)==1):
            spatemp = es.search(index = ind,size = 1000, body = {"query" : { "bool" : { "must" : 
                                                                            [
                                                                                {"range": { namespace+ path1 :  {opspacera[0] : critspacera[0]}}},
                                                                                {"range": { namespace+ path2 : {opspacedec[0] : critspacedec[0]}}},
                                                                                {"range": { namespace+ date:{optime[0] : crit_time[0]}}}
                                                                            ]    }}})            
        elif (len(opspacera)==2 and len(critspacera)==2 and len(opspacedec)==1 and len(critspacedec)==1 and len(optime)==1 and len(crit_time)==1):
            spatemp = es.search(index = ind,size = 1000, body = {"query" : { "bool" : { "must" : 
                                                                                       [
                                                                                           {"range": { namespace + path1 :  {opspacera[0] : critspacera[0], opspacera[1] : critspacera[1]}}},
                                                                                           {"range": { namespace + path2 : {opspacedec[0] : critspacedec[0]}}},
                                                                                           {"range": { namespace + date : {optime[0] : crit_time[0]}}}
                                                                                        ]    }}})
        elif (len(opspacera)==1 and len(critspacera)==1 and len(opspacedec)==2 and len(critspacedec)==2 and len(optime)==2 and len(crit_time)==2):
            spatemp = es.search(index = ind,size = 1000, body = {"query" : { "bool" : { "must" : 
                                                                                       [
                                                                                           {"range": { namespace+path1 :  {opspacera[0] : critspacera[0]}}},
                                                                                           {"range": { namespace+path2 : {opspacedec[0] : critspacedec[0],opspacedec[1] : critspacedec[1]}}},
                                                                                           {"range": { namespace+ date :{optime[0] : crit_time[0]}}}
                                                                                        ]    }}})
        else:
            return("problème d'argument")
        
        restotal += spatemp['hits']['total']['value']
        
        for hit in spatemp['hits']['hits']:
            spa = hit["_source"]
            spa = str(spa).replace("'","\"")
            spa = spa.replace("False", "false")
            spa = spa.replace("True", "true")
            product.append(spa)
            if not os.path.exists("/home/user/Documents/ResultatRecherche/" + ind):
                os.makedirs("/home/user/Documents/ResultatRecherche/" + ind)
            conversion.jsonstringtoxml(spa, "/home/user/Documents/ResultatRecherche/" + ind + "/" + str(hit['_id']) + ".xml")
            conversion.deleteitem("/home/user/Documents/ResultatRecherche/" +  ind+ "/" + str(hit['_id']) + ".xml")
    if restotal>0:
        print("fichiers de resultats stockes dans le dossier resultat recherche")
        return(str(restotal) + " Résultats trouvés")
        
    else :
        return(str(0)+" Résultats trouvés")
        





def recherche_temporelle(index, optime,crit_time):
    es = Elasticsearch()
    escat = CatClient(es)
    '''
    @namespace[j] est la racine du json dans lequel rechercher
    @optime est la liste contenant les opérateurs de comparaison portant sur la date recherché
    @crit_time est la liste des filtres définissant la date recherché
    '''
    restotal = 0
    date=""
    if (index=="*" or index=="_all"): 
        index = escat.indices(index = "_all", h="index").split("\n")
        del(index[-1])
    for ind in index:
        product = []
        namespace = getnamespacefromindex(ind)
        if ind in ("dpdnircalibratedframe", "dpdvisstackedframecatalog", "dpdviscalibratedframe"):
            date += ".Data.ObservationDateTime.UTC"
        elif ind in ("dpdextsingleepochframe", "dpdextcalibratedframe"):
            date += ".Data.ObservationDateTime.UTCObservationDateTime"
        else:
            date += "DateRange.TimestampStart"
        
        product = []
        if ind in ("dpdextstackedframe", "dpdsircombinedspectra", "dpdmersegmentationmap", "dpdmerfinalcatalog", "dpdmermosaic", "dpdphzpfoutputcatalog", "dpdmertile"):
            date = ""
        elif (len(optime)==2 and len(crit_time)==2):
            res_temp = es.search(index=ind,size = 1000, body ={"query": { "bool" : { "must" : {"range": {namespace+date:{optime[0] : crit_time[0], optime[1] : crit_time[1]}}}}}})
        elif (len(optime)==1 and len(crit_time)==1):
            res_temp = es.search(index=ind,size = 1000, body ={"query": { "bool" : { "must" : {"range": {namespace+date:{optime[0] : crit_time[0]}}}}}})
        else :
            return("problème d'argument")
        restotal += res_temp['hits']['total']['value']
        
        for hit in res_temp['hits']['hits']:
            spa = hit["_source"]
            spa = str(spa).replace("'","\"")
            spa = spa.replace("False", "false")
            spa = spa.replace("True", "true")
            product.append(spa)
            if not os.path.exists("/home/user/Documents/ResultatRecherche/"):
                os.makedirs("/home/user/Documents/ResultatRecherche/")
            conversion.jsonstringtoxml(spa, "/home/user/Documents/ResultatRecherche/" + ind + "/" + str(hit['_id']) + ".xml")
            conversion.deleteitem("/home/user/Documents/ResultatRecherche/" +  ind+ "/" + str(hit['_id']) + ".xml")
    if restotal>0:
        print("fichiers de resultats stockes dans le dossier resultat recherche")
        return(str(restotal) + " Résultats trouvés")
    else :
        return("Pas de resultat pour votre requete")



def recherchespatiale(index, opra,critra,opdec,critdec):
    es = Elasticsearch()
    escat = CatClient(es)
    '''
    @namespace[j] est la racine du json dans lequel rechercher
    @optime est la liste contenant les opérateurs de comparaison portant sur la date recherché
    @crit_time est la liste des filtres définissant la date recherché
    '''
    path1=""
    path2=""
    restotal = 0
    if len(opra)==0 and len (critra)==0:
        return recherchedec(index,opdec,critdec)
    elif len(opdec)==0 and len(critdec)==0:
        return recherchera(index,opra,critra)
    if (index=="*" or index=="_all"): 
        index = escat.indices(index = "_all", h="index").split("\n")
        del(index[-1])
    for i in range (0,len(index)):
        product = []
        ind = index[i]
        namespace = getnamespacefromindex(ind)
        if ind in ("dpdspepfoutputcatalog", "dpdvisstackedframecatalog", "dpdmerfinalcatalog", "dpdphzpfoutputcatalog"):
            path1 = ".Data.SpatialCoverage.Polygon.Vertex.Position.C1"
            path2 = ".Data.SpatialCoverage.Polygon.Vertex.Position.C2"
        elif ind =="dpdmertile":
            path1 = ".Data.InnerSpatialFootprint.Polygon.Vertex.Position.C1"
            path2 = ".Data.InnerSpatialFootprint.Polygon.Vertex.Position.C2"
            # todo : faire en sorte d'utiliser 2 path (Outer)
        elif "trueuniverseoutput" in ind :
            path1 = '.Data.Pointing.RA'
            path2 = '.Data.Pointing.Dec'
        else :
            path1 = ".Data.ImgSpatialFootprint.Polygon.Vertex.Position.C1"
            path2 = ".Data.ImgSpatialFootprint.Polygon.Vertex.Position.C2"
        if (len(opra)==2 and len(critra)==2 and len(opdec)==2 and len(critdec)==2):
            res_spa = es.search(index=ind,size = 1000, body= {"query" : { "bool" : { "must" :
                                                                                     [
                                                                                         {"range" : { namespace+path1:{opra[0]: critra[0] , opra[1] :critra[1]}}},
                                                                                         {"range" : { namespace+path2:{opdec[0] : critdec[0], opdec[1] : critdec[1]}}}
                                                                                        ]}}})
        elif (len(opra)==2 and len(critra)==2 and len(opdec)==1 and len(critdec)==1):
            res_spa = es.search(index=ind,size = 1000, body= {"query" : { "bool" : { "must" :
                                                                                     [
                                                                                        { "range" : { namespace+path1:{opra[0]: critra[0] , opra[1] :critra[1]}}},
                                                                                        {"range" : { namespace+path2:{opdec[0] : critdec[0]}}}
                                                                                        ] }}})
        elif (len(opra)==1 and len(critra)==1 and len(opdec)==1 and len(critdec)==1):
            res_spa = es.search(index=ind,size = 1000, body= {"query" : { "bool" : { "must" :
                                                                                     [
                                                                                        { "range" : { namespace+path1:{opra[0]: critra[0]}}},
                                                                                        {"range" : {namespace+path2:{opdec[0] : critdec[0]}}}
                                                                                        ] }}})
        elif (len(opra)==1 and len(critra)==1 and len(opdec)==2 and len(critdec)==2):
            res_spa = es.search(index=ind,size = 1000, body= {"query" : { "bool" : { "must" :
                                                                                     [
                                                                                        { "range" : { namespace+path1:{opra[0]: critra[0]}}},
                                                                                        {"range" : { namespace+path2:{opdec[0] : critdec[0], opdec[1] : critdec[1]}}}] }}})
        else:
            return("probleme avec le nombre d'arguments")
        restotal+= res_spa['hits']['total']['value']
                
        for hit in res_spa['hits']['hits']:
            prod = hit["_source"]
            prod = str(prod).replace("'","\"") 
            product.append(prod)
            prod = prod.replace("False", "false")
            prod = prod.replace("True", "true")
            if not os.path.exists("/home/user/Documents/ResultatRecherche/" + ind):
                os.makedirs("/home/user/Documents/ResultatRecherche/" + ind)
            conversion.jsonstringtoxml(prod, "/home/user/Documents/ResultatRecherche/" + ind + "/" + str(hit['_id']) + ".xml")
            conversion.deleteitem("/home/user/Documents/ResultatRecherche/" +  ind+ "/" + str(hit['_id']) + ".xml")

    if restotal>0:
        print("fichiers de resultats stockes dans le dossier resultat recherche")
        return(str(restotal) + " Résultats trouvés")
    else :
        return("0 fichier ne correspond à votre requete")

       
def recherchedec(index,opdec,critdec):
    es = Elasticsearch()
    escat = CatClient(es)
    path2=""
    restotal = 0
    if (index=="*" or index=="_all"): 
        index = escat.indices(index = "_all", h="index").split("\n")
        del(index[-1])
    for j in range (0,len(index)):
        product = []
        ind = index[j]
        namespace = getnamespacefromindex(ind)
        if ind in ("dpdspepfoutputcatalog", "dpdvisstackedframecatalog", "dpdmerfinalcatalog", "dpdphzpfoutputcatalog"):
            path2 = ".Data.SpatialCoverage.Polygon.Vertex.Position.C2"
        elif ind =="dpdmertile":
            path2 = ".Data.InnerSpatialFootprint.Polygon.Vertex.Position.C2"
        elif "trueuniverseoutput" in ind :
            path2 = '.Data.Pointing.Dec'
        else :
            path2 = ".Data.ImgSpatialFootprint.Polygon.Vertex.Position.C2"
            
            
        if (len(opdec)==2 and len(critdec)==2):
            res_spa = es.search(index=ind,size = 1000, body= {"query" : { "range" : { namespace+path2:{opdec[0] : critdec[0], opdec[1] : critdec[1]} }}})
        elif (len(opdec)==1 and len(critdec)==1):
            res_spa = es.search(index=ind,size = 1000, body= {"query" : { "range" : { namespace+path2:{opdec[0] : critdec[0]} }}})
        else:
            return("probleme avec le nombre d'arguments")
        
        restotal += res_spa['hits']['total']['value']
        
        for hit in res_spa['hits']['hits']:
            prod = hit["_source"]
            prod = str(prod).replace("'","\"") 
            prod = prod.replace("False", "false")
            prod = prod.replace("True", "true")
            product.append(prod)
            if not os.path.exists("/home/user/Documents/ResultatRecherche/" + ind):
                os.makedirs("/home/user/Documents/ResultatRecherche/" + ind)
            conversion.jsonstringtoxml(prod, "/home/user/Documents/ResultatRecherche/" + ind + "/" + str(hit['_id']) + ".xml")
            conversion.deleteitem("/home/user/Documents/ResultatRecherche/" +  ind+ "/" + str(hit['_id']) + ".xml")
    if restotal>0:
        print("fichiers de resultats stockes dans le dossier resultat recherche")
        return(str(restotal) + " Résultats trouvés")
    else :
        return("Pas de resultat pour votre requete")
    return "0"

def recherchera(index,opra,critra):
    es = Elasticsearch()
    escat = CatClient(es)
    path1=""
    restotal = 0
    if (index=="*" or index=="_all"): 
        index = escat.indices(index = "_all", h="index").split("\n")
        del(index[-1])
    for ind in index:
        product = []
        namespace = getnamespacefromindex(ind)
        if ind in ("dpdspepfoutputcatalog", "dpdvisstackedframecatalog", "dpdmerfinalcatalog", "dpdphzpfoutputcatalog"):
            path1 = ".Data.SpatialCoverage.Polygon.Vertex.Position.C1"
        elif ind =="dpdmertile":
            path1 = ".Data.InnerSpatialFootprint.Polygon.Vertex.Position.C1"
            # todo : faire en sorte d'utiliser 2 path (Outer)
        elif "trueuniverseoutput" in ind :
            path1 = '.Data.Pointing.RA'
        else :
            path1 = ".Data.ImgSpatialFootprint.Polygon.Vertex.Position.C1"
        if (len(opra)==2 and len(critra)==2):
            res_spa = es.search(index=ind,size = 1000, body= {"query" : { "range" : { namespace+path1:{opra[0]: critra[0] , opra[1] :critra[1]}} }})
        elif (len(opra)==1 and len(critra)==1):
            res_spa = es.search(index=ind,size = 1000, body= {"query" : { "range" : { namespace+path1:{opra[0]: critra[0]}} }})
        else:
            return("probleme avec le nombre d'arguments")
        restotal += res_spa['hits']['total']['value']
        for hit in res_spa['hits']['hits']:
            prod = hit["_source"]
            prod = str(prod).replace("'","\"") 
            prod = prod.replace("False", "false")
            prod = prod.replace("True", "true")
            product.append(prod)
            if not os.path.exists("/home/user/Documents/ResultatRecherche/"):
                os.makedirs("/home/user/Documents/ResultatRecherche/")
            conversion.jsonstringtoxml(prod, "/home/user/Documents/ResultatRecherche/" +  ind + ".xml")
            conversion.deleteitem("/home/user/Documents/ResultatRecherche/" +  ind+str(hit['_id']) + ".xml")
    if restotal>0:
        print("fichiers de resultats stockes dans le dossier resultat recherche")
        return(str(restotal) + " Résultats trouvés")
    else :
        return("Pas de resultat pour votre requete")
    return "0"

def matchall(index):
    es = Elasticsearch()
    ind=0
    product=[]
    escat = CatClient(es)
    restotal = 0
    if (index=="*" or index=="_all"): 
        index = escat.indices(index = "_all", h="index").split("\n")
        del(index[-1])
        print(1)
    for ind in index:
        product = []
        res = es.search(index=ind,size = 10000, body={"query": {"match_all": {}}})
        restotal+= res['hits']['total']['value']
        for hit in res['hits']['hits']:
            prod = hit["_source"]
            prod = str(prod).replace("'","\"") 
            prod = prod.replace("False", "false")
            prod = prod.replace("True", "true")
            product.append(prod)
            if not os.path.exists("/home/user/Documents/ResultatRecherche/" + ind):
                os.makedirs("/home/user/Documents/ResultatRecherche/" + ind)
            conversion.jsonstringtoxml(prod, "/home/user/Documents/ResultatRecherche/" + ind + "/" + str(hit['_id']) + ".xml")
            conversion.deleteitem("/home/user/Documents/ResultatRecherche/" +  ind+ "/" + str(hit['_id']) + ".xml")
    return restotal
  
    
    
def getallfields(index):
    escat = CatClient(es)
    if (index=="*" or index=="_all" or index =="" or index=="all" or index==None): 
        index = escat.indices(index = "_all", h="index").split("\n")
        del(index[-1])
    for i in range (0,len(index)):
        fields = es.ic.get_mapping(index=index[i])
        file = open("/home/user/Documents/AllFields/" + index[i],"w")
        file.write(str(fields))

    

def writeQuery(fields, index, op, crit):
    fields = fields.lower()
    fields.split("&&")
    
    
    
    
def requeteEuclid(field =[],index = {},op = {},crit = {}):
    es = Elasticsearch()
    escat = CatClient(es)
    restotal = 0
    product = []
    if (index=="*" or index=="_all" or index =="" or index=="all"): 
        index = escat.indices(index = "_all", h="index").split("\n")
        del(index[-1])
    for field in field :
        if isinstance((index.get(field)),str):
            ind = index.get(field)
            ind = ind.lower()
            namespace = getnamespacefromindex(ind)
            if (len(op.get(field))==2 and len(crit.get(field))==2):
                res = es.search(index=ind,size = 10000, body= {"query" : { "range" : { namespace+'.'+field:{op.get(field)[0]: crit.get(field)[0] , op.get(field)[1] :crit.get(field)[1] }}}})
            elif (len(op.get(field))==1 and len(crit.get(field))==1):
                res = es.search(index=ind,size = 10000, body= {"query" : { "range" : { namespace+'.'+field:{op.get(field)[0]: crit.get(field)[0] }}}})
            else:
                return("probleme avec le nombre d'arguments")
            restotal += res['hits']['total']['value']
            for hit in res['hits']['hits']:
                prod = hit["_source"]
                prod = str(prod).replace("'","\"") 
                prod = prod.replace("False", "false")
                prod = prod.replace("True", "true")
                product.append(prod)
                if not os.path.exists("/home/user/Documents/ResultatRecherche/"+ind):
                    os.makedirs("/home/user/Documents/ResultatRecherche/"+ind)
                conversion.jsonstringtoxml(prod, "/home/user/Documents/ResultatRecherche/" +  ind + "/" + hit["_source"][namespace]['Header']['ProductId'] + ".xml")
                conversion.deleteitem("/home/user/Documents/ResultatRecherche/" +  ind + "/" + hit["_source"][namespace]['Header']['ProductId'] + ".xml")
        else:
            for ind in index.get(field) :
                ind = ind.lower()
                namespace = getnamespacefromindex(ind)
                if (len(op.get(field))==2 and len(crit.get(field))==2):
                    res = es.search(index=ind,size = 10000, body= {"query" : { "range" : { namespace+'.'+field:{op.get(field)[0]: crit.get(field)[0] , op.get(field)[1] :crit.get(field)[1] }}}})
                elif (len(op.get(field))==1 and len(crit.get(field))==1):
                    res = es.search(index=ind,size = 10000, body= {"query" : { "range" : { namespace+'.'+field:{op.get(field)[0]: crit.get(field)[0] }}}})
                else:
                    return("probleme avec le nombre d'arguments")
                restotal += res['hits']['total']['value']
                for hit in res['hits']['hits']:
                    prod = hit["_source"]
                    prod = str(prod).replace("'","\"") 
                    prod = prod.replace("False", "false")
                    prod = prod.replace("True", "true")
                    product.append(prod)
                    if not os.path.exists("/home/user/Documents/ResultatRecherche/"+ind):
                        os.makedirs("/home/user/Documents/ResultatRecherche/"+ind)
                    conversion.jsonstringtoxml(prod, "/home/user/Documents/ResultatRecherche/" +  ind + "/" + hit["_source"][namespace]['Header']['ProductId'] + ".xml")
                    conversion.deleteitem("/home/user/Documents/ResultatRecherche/" +  ind + "/" + hit["_source"][namespace]['Header']['ProductId'] + ".xml")
    if restotal>0:
        return(str(restotal) + " Résultats trouvés")
    else :
        return("Pas de resultat pour votre requete")
    return "0"
    
    
field =[]
field.append("Data.TileIndex")
field.append("Data.ObservationId")
index = {}
index["Data.TileIndex"] = ["dpdmertile"]
index["Data.ObservationId"] = ["dpdvisstackedframe"]
op = {}
op["Data.TileIndex"]= ["gte","lte"]
op["Data.ObservationId"] = ["gte","lte"]
crit = {}
crit["Data.TileIndex"] = [18,18]
crit["Data.ObservationId"] = [401,401]
#print(requeteEuclid(field,index,op,crit))


def clik(tupl):
    tx = []
    ty=[]
    opx=[]
    opy=[]
    if len(tupl)== 1:
        tupl.append(tupl[0])
        x,y=tupl[0]
        x-=1
        y-=1
        tupl[0]=x,y
    for i in (0,1):
        x,y = tupl[i]
        tx.append(int(x))
        ty.append(int(y))
    if tx[0]<tx[1]:
        opx.append("gte")
        opx.append("lte")
    else:
        opx.append("lte")
        opx.append("gte")
    if ty[0]<ty[1]:
        opy.append("gte")
        opy.append("lte")
    else:
        opy.append("lte")
        opy.append("gte")
    print("RA =",tx)
    print("Dec =",ty)
    recherchespatiale("*", opx, tx, opy, ty)

